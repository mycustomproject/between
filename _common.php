<?php
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	require './lib/curl/curl.php';
	$curl = $curl = new Curl;
	// $response = $curl->head($url, $vars = array());
	// $response = $curl->get($url, $vars = array()); # The Curl object will append the array of $vars to the $url as a query string
	// $response = $curl->post($url, $vars = array());
	// $response = $curl->put($url, $vars = array());
	// $response = $curl->delete($url, $vars = array());
	
	
	function RenderJson($ErrorCode = 0,$Message = '', $Data = array())
	{
		
		header('Content-type: application/json; charset=UTF-8');
		exit(json_encode(array(
			'ErrorCode' => $ErrorCode,
			'Message' => $Message,
			'Data' => $Data
		), 256));
	}