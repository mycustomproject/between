<?php 
    require_once './_common.php';

    $api_url = 'https://api.8008.company';
    $token = 'abcd';
    
    //쿼리스트링에 game과 table이 있는지 조사.
    $mode = isset($_GET['mode']) === false ? 'play' : htmlspecialchars(strtolower($_GET["mode"]));
    
    //모드가 플레이 일경우...
    if($mode == 'play')
    {
    	$curl->setUrl($api_url.'/v1/saisai/play');
    	$curl->setPostData(array(
    		'token' => $token,
    	));
    	$res = json_decode($curl->getResponse(), true);
    	
    	RenderJson(0,'Success', $res['Data']);
    }
    else if($mode == 'history')
    {
    	$curl->setUrl($api_url.'/v1/saisai/history');
    	$curl->setPostData(array(
    		'token' => $token
    	));
    	$res = json_decode($curl->getResponse(), true);
    	
    	RenderJson(0,'Success', $res['Data']);
    }
    else if($mode == 'result') 
    {
    	$curl->setUrl($api_url.'/v1/saisai/result');
    	$curl->setPostData(array(
    		'token' => $token
    	));
    	$res = json_decode($curl->getResponse());
    	
    	RenderJson($res->ErrorCode, $res->Message, $res->Data);	
    } else if($mode == 'chat') {
        $curl->setUrl($api_url.'/v1/chat/send');
        $curl->setPostData(array(
            'nickname' => $_POST['nickname'],
            'message' => $_POST['message'],
            'token' => $token
        ));
        $res = json_decode($curl->getResponse());

        RenderJson($res->ErrorCode, $res->Message); 
    }
?>