<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

<title>DEMO CLIENT</title>
<link rel="stylesheet" type="text/css" media="screen" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="assets/css/flipclock.css?v=<?php echo time() ?>">
<link rel="stylesheet" href="assets/css/style.css?v=<?php echo time() ?>">
<link rel="stylesheet" href="assets/css/baccarat/mobile.css?v=<?php echo time() ?>">
</head>
<body>
	<div class="bacc-mobile-wrap">
		<div class="video-box">
			<div class="message-box" style="background-image: url('assets/img/background/message_box.png')"></div>

			<canvas id="videoCanvas" style="width:100%"></canvas>
			<div class="card-box">
				<div class="card-players card-player-1" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time() ?>')"></div>
				<div class="card-players card-player-2" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time() ?>')"></div>
				<div class="card-players card-player-3" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time() ?>')"></div>

				<div class="card-bankers card-banker-1" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time() ?>')"></div>
				<div class="card-bankers card-banker-2" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time() ?>')"></div>
				<div class="card-bankers card-banker-3" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time() ?>')"></div>
			</div>

			<div class="count-box" id="count-down"></div>
			
			<div class="win-box">
				플레이어가 이겼습니다.
			</div>
		</div>

		<div class="chip-box-outer">
			<div class="chip-box-inner" style="background-image:url('assets/img/background/mobile_chip_box_background.png')">
				<div class="notice">
					<a href="http://between.com">
						<img src="/assets/img/background/bacMGame.png">
					</a>
				</div>
				<ul class="chip">
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="5000" data-location="1"  src="assets/img/chip/chip_1_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="10000" data-location="2"  src="assets/img/chip/chip_2_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="50000" data-location="3"  src="assets/img/chip/chip_3_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="100000" data-location="4"  src="assets/img/chip/chip_4_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="500000" data-location="5"  src="assets/img/chip/chip_5_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="1000000" data-location="6"  src="assets/img/chip/chip_6_off.png?v={=time ()}"></a></li>
				</ul>
			</div>
		</div>
		<!-- <div class="chip-box">
			<div class="scroll">
				<ul class="chip">
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="5000" data-location="1"  src="assets/img/chip/chip_1_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="10000" data-location="2"  src="assets/img/chip/chip_2_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="50000" data-location="3"  src="assets/img/chip/chip_3_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="100000" data-location="4"  src="assets/img/chip/chip_4_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="500000" data-location="5"  src="assets/img/chip/chip_5_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="1000000" data-location="6"  src="assets/img/chip/chip_6_off.png?v={=time ()}"></a></li>
				</ul>
			</div>
		</div> -->
<div class="betting-panel betting-overlay">
</div>
		<div class="betting-panel">
			<div class="btn-bet btn-player-pair btn-panel-4 win-pp" data-label="플레이어 페어" data-location="4">
				<div class="label">P Pair</div>
				<div class="rate">11.00</div>
				<div class="bet-money bet-money-4">0</div>
			</div>
			<div class="btn-bet btn-banker-pair btn-panel-5 win-bp" data-label="뱅커 페어" data-location="5">
				<div class="label">B Pair</div>
				<div class="rate">11.00</div>
				<div class="bet-money bet-money-5">0</div>
			</div>
			
			<div class="btn-bet btn-player btn-panel-1 win-p" data-label="플레이어"  data-location="1">
				<div class="label">Player <span class="rate">2.00</span></div>
				<div class="bet-money-box"></div>
				<div class="bet-money bet-money-1">0</div>
			</div>
			<div class="btn-bet btn-tie btn-panel-3 win-t" data-label="타이"  data-location="3">
				<div class="label">Tie <span class="rate">8:1</span></div>
				<div class="bet-money-box"></div>
				<div class="bet-money bet-money-3">0</div>
			</div>
			<div class="btn-bet btn-banker btn-panel-2 win-b" data-label="뱅커"  data-location="2">
				<div class="label">Banker <span class="rate">1.95</span></div>
				<div class="bet-money-box"></div>
				<div class="bet-money bet-money-2">0</div>
			</div>
		</div>
		
		<div class="btn-confirm-box">
			<div class="btn-confirm-box-one">
				<ul>
					<li class="btn-chip-confirm">
						<a href="javascript:void(0)" style="background-image:url('assets/img/background/mobile_btn_bet_confirm_one.png')">
							베팅확인
						</a>
					</li>
					<li class="btn-chip-reset">
						<a href="javascript:void(0)" style="background-image:url('assets/img/background/mobile_btn_bet_cancle_one.png')">
							금액초기화
						</a>
					</li>	
				</ul>
			</div>
			<div class="btn-confirm-box-two" style="background-image:url('assets/img/background/mobile_title_background.png')">
				<!-- <label>[<span class="location_label"></span>]에 베팅하시겠습니까?</label> -->
				<label>베팅하시겠습니까?</label>
				<div class="btn-chip-ok" style="background-image:url('assets/img/background/mobile_btn_bet_confirm_ok.png')">확인</div>
				<div class="btn-chip-cancel" style="background-image:url('assets/img/background/mobile_btn_bet_confirm_cancel.png')">취소</div>
			</div>
		</div>
		
		<div class="info-box">
			   <div class="livechat"><span>라이브채팅</span></div>
			   <div class="input-box-border"><input type="text" class="input-box-text" placeholder="입력한 글이 영상속 태블릿에 표현되어 라이브임을 인증 합니다."></div>
			   <div class="send-button">전송</div>

			<!--div class="bet-info-box-confirm">
				<div style="width:500px">
					<span>[<span>PP</span> - <span class="bet-log-4">0</span>] </span>
					<span>[<span>P</span> - <span class="bet-log-1">0</span>] </span>
					<span>[<span>T</span> - <span class="bet-log-3">0</span>] </span>
					<span>[<span>B</span>  - <span class="bet-log-2">0</span>] </span>
					<span>[<span>BP</span> - <span class="bet-log-5">0</span>] </span>
				</div>
			</div-->
		</div>

		<div class="nav-box">
			<ul>
				<li><a href="javascript:void(0)" id="show-history">출목표</a></li>
				<li><a href="javascript:void(0)" id="show-log">베팅로그</a></li>
				<li><a href="javascript:void(0)" id="show-official">공식결과</a></li>
			</ul>
		</div>

		<div class="history-box">
			<div class="history-title">
				<div class="history-label">원매</div>
			</div>
			<div class="history-all detail-history-box" id="history-detail" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">6매</div>
			</div>
			<div class="history-all simple-history-box" id="history-simple" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">3매</div>
			</div>
			<div class="history-all simple-detail-history-box" id="history-simple-detail" style="background-color:#fff">
			</div>
			<!--div class="history-title">
				<div class="history-label">중국점 1군</div>
			</div>
			<div class="history-all ch-history-box detail-1-history-box" id="history-detail-1" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">중국점 2군</div>
			</div>
			<div class="history-all ch-history-box detail-2-history-box" id="history-detail-2" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">중국점 3군</div>
			</div>
			<div class="history-all ch-history-box detail-3-history-box" id="history-detail-3" style="background-color:#fff">
			</div-->
		</div>

		<div class="official-box" style="display:none">
		</div>

		<div class="log-box" style="display:none">
			<div class="log-list-box" id="log-list-box">
				<ul>
					
				</ul>
			</div>
		</div>
	</div>
	<script>
		var CONFIG = {
			ASSETS : '/assets',
			AJAX : '/json.php',
			MEMBER : {
				IS_LOGIN:<?php echo $member == null ? 'false' : 'true'?>,
				NICKNAME : '게스트',
				MONEY : 100000000
			},
			GAME : {
				MIN : 10000,
				MAX : 10000000
			}
		}
	</script>
	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/underscore.js"></script>
	<script src="assets/js/function.js?v=<?php echo time() ?>"></script>
	<script src="assets/js/mpeg.js?v=<?php echo time() ?>"></script>
	<script src="assets/js/baccarat/mobile.js?v=<?php echo time() ?>"></script>
</body>
</html>
