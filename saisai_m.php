<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

<title>DEMO CLIENT</title>
<link rel="stylesheet" type="text/css" media="screen" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="assets/css/flipclock.css?v=<?php echo time();?>">
<link rel="stylesheet" href="assets/css/style.css?v=<?php echo time();?>">
<link rel="stylesheet" href="assets/css/saisai/mobile.css?v=<?php echo time();?>">
</head>
<body>
	<div class="saisai-mobile-wrap">

		<div class="video-box">
			<div class="message-box" style="background-image: url('assets/img/background/message_box.png')"></div>
			
			<canvas id="videoCanvas" style="width:100%"></canvas>

			<div class="card-box">
				<div class="cards left-card" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time();?>')"></div>
				<div class="cards center-card" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time();?>')"></div>
				<div class="cards right-card" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time();?>')"></div>
			</div>

			<div class="count-box" id="count-down"></div>

			
			<div class="win-box"></div>
		</div>

		
		<div class="chip-box-outer">
			<div class="chip-box-inner" style="background-image:url('assets/img/background/mobile_chip_box_background.png')">
				<div class="notice">
					<a href="http://between.com">
						<img src="/assets/img/background/saisaiMGame.png">
					</a>
				</div>
				<ul class="chip">
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="5000" data-location="1"  src="assets/img/chip/chip_1_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="10000" data-location="2"  src="assets/img/chip/chip_2_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="50000" data-location="3"  src="assets/img/chip/chip_3_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="100000" data-location="4"  src="assets/img/chip/chip_4_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="500000" data-location="5"  src="assets/img/chip/chip_5_off.png?v={=time ()}"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="1000000" data-location="6"  src="assets/img/chip/chip_6_off.png?v={=time ()}"></a></li>
				</ul>
			</div>
		</div>
		
		<div class="betting-panel betting-overlay">
		</div>
		
		<div class="betting-panel">
			
			<div class="btn-bet btn-odd btn-panel-11 win-o" data-label="홀" data-location="11">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-11">0</div>
				<div class="rate-label rate-label-11">ODD(홀) <span class="rate-label-rate">1.95</span></div>
			</div>
			
			<div class="btn-bet btn-even btn-panel-12 win-e" data-label="짝" data-location="12">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-12">0</div>
				<div class="rate-label rate-label-12">EVEN(짝) <span class="rate-label-rate">1.95</span></div>
			</div>
			
			<div class="btn-bet btn-black btn-panel-25 win-c-b" data-label="검정" data-location="25">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-25">0</div>
				<div class="rate-label rate-label-25">BLACK(검정) <span class="rate-label-rate">1.95</span></div>
			</div>
			
			<div class="btn-bet btn-red btn-panel-24 win-c-r" data-label="빨강" data-location="24">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-24">0</div>
				<div class="rate-label rate-label-1">RED(빨강) <span class="rate-label-rate">1.95</span></div>
			</div>
			
			
			<div class="btn-bet btn-special btn-panel-0 win-0" data-label="사이잡기" data-location="0">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-0">0</div>
				<div class="rate-label rate-label-4">x <span class="rate-label-rate special-rate">배당없음</span></div>
				<div class="center-number">[?]</div>
				<div class="left-number">?</div>
				<div class="right-number">?</div>
			</div>
			
			
			<div class="btn-bet btn-spade btn-panel-13 win-s" data-label="스페이드" data-location="13">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-13">0</div>
				<div class="rate-label rate-label-2">Spade(스페이드) <span class="rate-label-rate">3.70</span></div>
			</div>

			
			<div class="btn-bet btn-diamond btn-panel-14 win-d" data-label="다이아몬드" data-location="14">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-14">0</div>
				<div class="rate-label rate-label-2">Dia(다이아) <span class="rate-label-rate">3.70</span></div>
			</div>

			
			<div class="btn-bet btn-heart btn-panel-15 win-h" data-label="하트" data-location="15">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-15">0</div>
				<div class="rate-label rate-label-2">Heart(하트) <span class="rate-label-rate">3.70</span></div>
			</div>

			
			<div class="btn-bet btn-clover btn-panel-16 win-c" data-label="크로바" data-location="16">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-16">0</div>
				<div class="rate-label rate-label-2">Clover(클로버) <span class="rate-label-rate">3.70</span></div>
			</div>

			
			<div class="btn-bet btn-low btn-panel-21 win-low" data-label="소(A~4)" data-location="21">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-21">0</div>
				<div class="rate-label rate-label-3">Low(A~4) <span class="rate-label-rate">2.7</span></div>
			</div>

			
			<div class="btn-bet btn-middle btn-panel-22 win-middle" data-label="중(5~8)" data-location="22">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-22">0</div>
				<div class="rate-label rate-label-3">Middle(5~8) <span class="rate-label-rate">2.7</span></div>
			</div>

			
			<div class="btn-bet btn-high btn-panel-23 win-high" data-label="대(9~Q)" data-location="23">
				<div class="bet-money-back"></div>
				<div class="bet-money bet-money-23">0</div>
				<div class="rate-label rate-label-3">High(9~Q) <span class="rate-label-rate">2.7</span></div>
			</div>
		</div>
		
		
		<div class="btn-confirm-box">
			<div class="btn-confirm-box-one">
				<ul>
					<li class="btn-chip-confirm">
						<a href="javascript:void(0)" style="background-image:url('assets/img/background/mobile_btn_bet_confirm_one.png')">
							베팅확인
						</a>
					</li>
					<li class="btn-chip-reset">
						<a href="javascript:void(0)" style="background-image:url('assets/img/background/mobile_btn_bet_cancle_one.png')">
							금액초기화
						</a>
					</li>	
				</ul>
			</div>
			<div class="btn-confirm-box-two" style="background-image:url('assets/img/background/mobile_title_background.png')">
				<!-- <label>[<span class="location_label"></span>]에 베팅하시겠습니까?</label> -->
				<label>베팅하시겠습니까?</label>
				<div class="btn-chip-ok" style="background-image:url('assets/img/background/mobile_btn_bet_confirm_ok.png')">확인</div>
				<div class="btn-chip-cancel" style="background-image:url('assets/img/background/mobile_btn_bet_confirm_cancel.png')">취소</div>
			</div>
		</div>
		<div class="info-box">
			   <div class="livechat"><span>라이브채팅</span></div>
			   <div class="input-box-border"><input type="text" class="input-box-text" placeholder="입력한 글이 영상속 태블릿에 표현되어 라이브임을 인증 합니다."></div>
			   <div class="send-button">전송</div>

			<!--div class="bet-info-box-confirm">
				<div style="width:500px">
					<span>[<span>PP</span> - <span class="bet-log-4">0</span>] </span>
					<span>[<span>P</span> - <span class="bet-log-1">0</span>] </span>
					<span>[<span>T</span> - <span class="bet-log-3">0</span>] </span>
					<span>[<span>B</span>  - <span class="bet-log-2">0</span>] </span>
					<span>[<span>BP</span> - <span class="bet-log-5">0</span>] </span>
				</div>
			</div-->
		</div>
		
		<div class="nav-box">
			<ul>
				<li><a href="javascript:void(0)" id="show-history">출목표</a></li>
				<li><a href="javascript:void(0)" id="show-log">베팅로그</a></li>
				<li><a href="javascript:void(0)" id="show-official">공식결과</a></li>
			</ul>
		</div>

		
		<div class="history-box">
			
			<div class="history-title">
				<div class="history-label">원매</div>
			</div>
			<div class="history-all detail-history-box" id="history-detail" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">6매</div>
			</div>
			<div class="history-all simple-history-box" id="history-simple" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">3매</div>
			</div>
			<div class="history-all simple-detail-history-box" id="history-simple-detail" style="background-color:#fff">
			</div>
			<!--div class="history-title">
				<div class="history-label">중국점 1군</div>
			</div>
			<div class="history-all ch-history-box detail-1-history-box" id="history-detail-1" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">중국점 2군</div>
			</div>
			<div class="history-all ch-history-box detail-2-history-box" id="history-detail-2" style="background-color:#fff">
			</div>
			<div class="history-title">
				<div class="history-label">중국점 3군</div>
			</div>
			<div class="history-all ch-history-box detail-3-history-box" id="history-detail-3" style="background-color:#fff">
			</div-->
		</div>

		
		<div class="chat-box" style="display:none">
			<div class="input-box-border">
				<input type="text" class="input-box-text" placeholder="입력한 글이 영상속 태블릿에 표현되어 라이브임을 인증 합니다.">
			</div>
			
			<div class="send-button">전송</div>
			
			<div class="chat-list-box" id="chat-list-box">
				<ul>
					
				</ul>
			</div>
		</div>

		
		<div class="log-box" style="display:none">
			<div class="log-list-box" id="log-list-box">
				<ul>
					
				</ul>
			</div>
		</div>
	</div>
	<script>
		var CONFIG = {
			ASSETS : '/assets',
			AJAX : '/json.php',
			MEMBER : {
				IS_LOGIN:<?php echo $member == null ? 'false' : 'true'?>,
				NICKNAME : '게스트',
				MONEY : 100000000
			},
			GAME : {
				MIN : 10000,
				MAX : 10000000
			}
		}
	</script>
	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/underscore.js"></script>
	<script src="assets/js/function.js?v=<?php echo time();?>"></script>
	<script src="assets/js/mpeg.js?v=<?php echo time();?>"></script>
	<script src="assets/js/saisai/mobile.js?v=<?php echo time();?>"></script>
</body>
</html>
