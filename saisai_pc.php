<?php
	$usr_agent = $_SERVER['HTTP_USER_AGENT'];
	$is_iphone = preg_match("~\biphone\b~",strtolower($usr_agent));
	$is_ipad = preg_match("~\bipad\b~",strtolower($usr_agent));
	$is_android = preg_match("~\bandroid\b~",strtolower($usr_agent));
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="Content-Security-Policy" content="default-src * data:; style-src * 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
<title>DEMO CLIENT</title>
<link rel="stylesheet" type="text/css" media="screen" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="assets/css/flipclock.css?v=<?php echo time();?>">
<link rel="stylesheet" href="assets/css/style.css?v=<?php echo time();?>">
<link rel="stylesheet" href="assets/css/saisai/pc.css?v=<?php echo time();?>">

</head>
<body>
	<div class="saisai-wrap bg-color-black">
		<div class="video">
			<?php if($is_iphone == 0 && $is_ipad == 0 && $is_android == 0) {?>
			<object id="video_swf" type="application/x-shockwave-flash" data="assets/swf/onecard.swf" width="100%" height="100%">
				<param name="movie" value="assets/swf/baccarat.swf">
				<param name="quality" value="high">
				<param name="bgcolor" value="#101010">
				<param name="play" value="true">
				<param name="loop" value="true">
				<param name="wmode" value="window">
				<param name="scale" value="showall">
				<param name="menu" value="true">
				<param name="devicefont" value="false">
				<param name="salign" value="">
				<param name="allowScriptAccess" value="sameDomain">
			</object>
			<?php } else {?>
			<canvas id="videoCanvas" class="video-mobile"></canvas>
			<script src="assets/js/mpeg.js"></script>
			<script>
				var canvas = document.getElementById('videoCanvas');
				var ctx = canvas.getContext('2d');
				ctx.fillStyle = '#444';
				ctx.fillText('Stream loading...', canvas.width/2-30, canvas.height/3);
				var client = new WebSocket( 'wss://video2.8008.company/' );
				var player = new jsmpeg(client, {canvas:canvas});
			</script>
			<?php }?>
			<div class="message-box" style="background-image: url('assets/img/background/message_box.png')"></div>
			<div id="count-down"></div>
			<div class="chat-list-box" id="chat-list-box"><ul></ul></div>
		</div>
		
		<!--div class="saisai-title">
			<div class="title">
				(<a href="javascript:void(0)" class="show-bet-panel">베팅판보기</a>) - BETWEEN SAISAI -(<a href="javascript:void(0)" class="show-card-box">카드보기</a>)
			</div>
			<div class="history-show">
				출목표 통계
			</div>
		</div-->
		<div class="baccarat-title">
			<div class="buttonBox">
				<div class="history-show button">
					출목표 통계
				</div>
				<div class="toggle-button button">
					배팅 로그
				</div>
				<div class="official-show button">
					공식 결과
				</div>
			</div>
			<div class="title">
				<strong>[비트윈]</strong>님의 보유머니 : 1,000,000,000원
			</div>
			<div class="buttonBox">
				<div class="show-card-box" style="cursor:pointer">
					<img src="/assets/img/button/saisai/cardShow.png">
				</div>
				<div class="show-bet-panel" style="cursor:pointer">
					<img src="/assets/img/button/saisai/betShow.png">
				</div>
				<div class="show-desc-game" style="cursor:pointer">
					<img src="/assets/img/button/saisai/gameDesc.png">
				</div>
			</div>
		</div>
		<div class="history-box">
			<div class="simple-history-box" id="history-simple" style="background-image:url('assets/img/history/background.png?v=<?php echo time()?>')">
			</div>
			<div class="detail-history-box" id="history-detail" style="background-image:url('assets/img/history/background.png?v=<?php echo time();?>')">
			</div>
			<div class="simple-detail-history-box" id="history-simple-detail" style="background-image:url('assets/img/history/background.png?v=<?php echo time()?>')">
			</div>
		</div>
		<div class="saisai-chat-box">
			<div class="title">
				비트윈 라이브 채팅
			</div>
			<div class="input-box">
				<input type="text" maxlength="8" placeholder="(해당 기능은 비트윈 바카라가 라이브임을 인증하는 기능이며, 실제 현장과의 딜레이는 3초이내이기 때문에 조작이 불가능합니다.)" class="input-box-text">
			</div>
			<div class="send-button">
				전송
			</div>
		</div>

		<div class="game-desc"><img src="/assets/img/background/saisai_game.png"></div>
		<div class="chip-box-one">
		<div class="chip-bw">
				<a href="http://betweenapi.com" target="_blank">
					<img src="/assets/img/background/bwSaisai.png">
				</a>
			</div>
			<div class="chip-box">
				<ul class="chip">
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="5000" data-location="1"  src="assets/img/chip/chip_1_off.png?v=<?php echo time();?>"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="10000" data-location="2"  src="assets/img/chip/chip_2_off.png?v=<?php echo time();?>"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="50000" data-location="3"  src="assets/img/chip/chip_3_off.png?v=<?php echo time();?>"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="100000" data-location="4"  src="assets/img/chip/chip_4_off.png?v=<?php echo time();?>"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="500000" data-location="5"  src="assets/img/chip/chip_5_off.png?v=<?php echo time();?>"></a></li>
					<li><a href="javascript:void(0)"><img class="chip-btn" data-money="1000000" data-location="6"  src="assets/img/chip/chip_6_off.png?v=<?php echo time();?>"></a></li>
				</ul>
			</div>
			<div class="confirm-button-box">
				<ul>
					<li>
						<a href="javascript:void(0)" class="btn-chip-confirm">
							<img src="assets/img/button/saisai/btn_bet_confirm_one.png?v=<?php echo time();?>">
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="btn-chip-reset">
							<img src="assets/img/button/saisai/btn_bet_cancel_one.png?v=<?php echo time();?>">
						</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="chip-box-two">
			<div class="label-box">
				<img src="assets/img/background/label_two_confirm.png?v=<?php echo time();?>">
			</div>
			<div class="confirm-button-box">
				<ul>
					<li>
						<a href="javascript:void(0)" class="btn-chip-ok">
							<img class="second-btn-box" src="assets/img/button/saisai/bet_confirm_off.png?v=<?php echo time();?>">
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="btn-chip-cancel">
							<img class="second-btn-box" src="assets/img/button/saisai/bet_cancel_off.png?v=<?php echo time();?>">
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="official-list"></div>
		<div class="betting-panel">
			<div class="betting-btn-box">
				
				<div class="btn-bet btn-odd btn-panel-11 win-o" style="background-image:url('assets/img/button/saisai/odd_off.png?v=<?php echo time();?>')" data-label="홀" data-location="11">
					<div class="bet-money bet-money-11" id="bet-money-11">0</div>
					<div class="rate-label-1">x <span class="rate-label-rate">1.95</span></div>
				</div>
				
				<div class="btn-bet btn-even btn-panel-12 win-e" style="background-image:url('assets/img/button/saisai/even_off.png?v=<?php echo time();?>')" data-label="짝" data-location="12">
					<div class="bet-money bet-money-12" id="bet-money-12">0</div>
					<div class="rate-label-1">x <span class="rate-label-rate">1.95</span></div>
				</div>
				
				<div class="btn-bet btn-black btn-panel-25 win-c-b" style="background-image:url('assets/img/button/saisai/black_off.png?v=<?php echo time();?>')" data-label="검정" data-location="25">
					<div class="bet-money bet-money-25" id="bet-money-25">0</div>
					<div class="rate-label-1">x <span class="rate-label-rate">1.95</span></div>
				</div>
				
				<div class="btn-bet btn-red btn-panel-24 win-c-r" style="background-image:url('assets/img/button/saisai/red_off.png?v=<?php echo time();?>')" data-label="빨강" data-location="24">
					<div class="bet-money bet-money-24" id="bet-money-24">0</div>
					<div class="rate-label-1">x <span class="rate-label-rate">1.95</span></div>
				</div>
				
				<div class="btn-bet btn-special btn-panel-0 win-0" style="background-image:url('assets/img/button/saisai/special_off.png?v=<?php echo time();?>')" data-label="사이잡기" data-location="0">
					<div class="bet-money bet-money-0" id="bet-money-0">0</div>
					<div class="rate-label-4">x <span class="rate-label-rate special-rate">배당없음</span></div>
					<div class="left-number">?</div>
					<div class="right-number">?</div>
				</div>
				
				
				<div class="btn-bet btn-spade btn-panel-13 win-s" style="background-image:url('assets/img/button/saisai/spade_off.png?v=<?php echo time();?>')" data-label="스페이드" data-location="13">
					<div class="bet-money bet-money-13" id="bet-money-13">0</div>
					<div class="rate-label-2">x <span class="rate-label-rate">3.7</span></div>
				</div>

				
				<div class="btn-bet btn-diamond btn-panel-14 win-d" style="background-image:url('assets/img/button/saisai/diamond_off.png?v=<?php echo time();?>')" data-label="다이아몬드" data-location="14">
					<div class="bet-money bet-money-14" id="bet-money-14">0</div>
					<div class="rate-label-2">x <span class="rate-label-rate">3.7</span></div>
				</div>

				
				<div class="btn-bet btn-heart btn-panel-15 win-h" style="background-image:url('assets/img/button/saisai/heart_off.png?v=<?php echo time();?>')" data-label="하트" data-location="15">
					<div class="bet-money bet-money-15" id="bet-money-15">0</div>
					<div class="rate-label-2">x <span class="rate-label-rate">3.7</span></div>
				</div>

				
				<div class="btn-bet btn-clover btn-panel-16 win-c" style="background-image:url('assets/img/button/saisai/clover_off.png?v=<?php echo time();?>')" data-label="크로바" data-location="16">
					<div class="bet-money bet-money-16" id="bet-money-16">0</div>
					<div class="rate-label-2">x <span class="rate-label-rate">3.7</span></div>
				</div>

				
				<div class="btn-bet btn-low btn-panel-21 win-low" style="background-image:url('assets/img/button/saisai/low_off.png?v=<?php echo time();?>')" data-label="소(A~4)" data-location="21">
					<div class="bet-money bet-money-21" id="bet-money-21">0</div>
					<div class="rate-label-3">x <span class="rate-label-rate">2.7</span></div>
				</div>

				
				<div class="btn-bet btn-middle btn-panel-22 win-middle" style="background-image:url('assets/img/button/saisai/middle_off.png?v=<?php echo time();?>')" data-label="중(5~8)" data-location="22">
					<div class="bet-money bet-money-22" id="bet-money-22">0</div>
					<div class="rate-label-3">x <span class="rate-label-rate">2.7</span></div>
				</div>

				
				<div class="btn-bet btn-high btn-panel-23 win-high" style="background-image:url('assets/img/button/saisai/high_off.png?v=<?php echo time();?>')" data-label="대(9~Q)" data-location="23">
					<div class="bet-money bet-money-23" id="bet-money-23">0</div>
					<div class="rate-label-3">x <span class="rate-label-rate">2.7</span></div>
				</div>
			</div>

			<div class="card-box-blind"></div>
			<div class="card-box">
				<div class="cards left-card" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time();?>')"></div>
				<div class="cards center-card" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time();?>')"></div>
				<div class="cards right-card" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time();?>')"></div>
			</div>
		</div>
	</div>
	<script>
		var CONFIG = {
			ASSETS : '/assets',
			AJAX : '/json.php',
			MEMBER : {
				IS_LOGIN:<?php echo $member == null ? 'false' : 'true'?>,
				NICKNAME : '게스트',
				MONEY : 100000000
			},
			GAME : {
				MIN : 10000,
				MAX : 10000000
			}
		}
	</script>
	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/underscore.js"></script>
	<script src="assets/js/count.js"></script>
	<script src="assets/js/function.js?v=<?php echo time();?>"></script>
	<script src="assets/js/mpeg.js?v=<?php echo time();?>"></script>
	<script src="assets/js/saisai/pc.js?v=<?php echo time();?>"></script>
</body>
</html>
