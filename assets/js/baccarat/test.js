var c_point=[];
var j_point=[];
var c_point_sw = 0;
var c_point_count = 0;
var c_point_result = 0;
var c_point_cnt = 0;
var c_point_v = -1;
var last_result = null;

function c_1() {
	var result = {"win":"b","pair":""};
    var data = history_pre_data;
    if (data == null) { 
        return;
    }
    RenderHistorySimpleTemp(result);
	RenderHistoryDetailTemp(result);
}

function c_2() {
	var result = {"win":"p","pair":""};
    var data = history_pre_data;
    if (data == null) { 
        return;
    }
    RenderHistorySimpleTemp(result);
	RenderHistoryDetailTemp(result);
}

//예상 히스토리 그려주기(일반 심플)
function RenderHistorySimpleTemp (result) {
    if (result.pair == 'no') {
		result.pair = '';
	}
    
    //일반 히스토리 x와 y 값을 따로 변수로 지정 받아둔다.
    if (simple_y == 5){
        var x = 0;
        var y = simple_y + 1;
    } else {
        var x = simple_x + 1;
        var y = simple_y;
    }

	var simple_img = 'simple_'+result.win.toLowerCase() + '.png?v=300';		
	
	$("#history-simple-" + y + "-" + x).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+simple_img+'\')"></div>');
    
    var animation = setInterval(function() {
		if ($("#history-simple-" + y + "-" + x + " *").css('opacity') == '1') {
			$("#history-simple-" + y + "-" + x + " *").fadeOut('slow');
		} else {
			$("#history-simple-" + y + "-" + x + " *").fadeIn('slow');
		}
	});
	setTimeout(function() {
		clearInterval(animation);
		$("#history-simple-" + y + "-" + x + " *").fadeOut();
	},1000);
}

function RenderHistoryDetailTemp (result) {
	//{"win":"p","pair":"","big":9}
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data;
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=300';
		
		$("#history-detail-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			x = pre_data.x - (temp_history_plus - 1);
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=300';
		
		$("#history-detail-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
	
	////////////////////////////////////////////////////////////////////////
	//중국점 추가를 위해 테스트 변수(매번 초기화 시켜주는게 궁극임.
	// var c_point = JSON.parse(JSON.stringify(c_point));
	// var j_point = JSON.parse(JSON.stringify(j_point));
	

	if(result.win == 'T') {
		return;
	} else {
		if (last_result != result.win) {
			j_point = 0;
			j_point = j_point + 1;
			c_point.push(j_point);
			c_point_v = c_point_v + 1
		}

		//내용이 같은것은 아래로~
		if (last_result == result.win) {
			j_point = j_point + 1;
			c_point.pop();
			c_point.push(j_point);
		}
		last_result = result.win;
	}
	
	//중국점 1번 을 그려줍세
	if(c_point_sw == 1) {	
		if (c_point_v[c_point_v+1] > 0) {
			
			c_point_v = c_point_v + 1;
		}	
		
		if(c_point[c_point_v] > 1) {
			c_point_cnt = c_point[c_point_v - 1] - c_point[c_point_v];
			
			if (c_point_cnt == -1) {
				RenderHistoryDetailTemp1({'win':'p','pair':'no'});
			} else {
				RenderHistoryDetailTemp1({'win':'b','pair':'no'});
			}
		} else {
			c_point_cnt = c_point[c_point_v - 1] - c_point[c_point_v - 2];
			
			if (c_point_cnt == 0) {
				RenderHistoryDetailTemp1({'win':'b','pair':'no'});
			} else {
				RenderHistoryDetailTemp1({'win':'p','pair':'no'});
			}
		}
	} else {
		//sw = 0
		if (c_point[1] > 0) {
			c_point_sw = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt = c_point[c_point_v-1] - i;
				
				if (c_point_cnt > 0 || c_point_cnt > -1) {
					RenderHistoryDetailTemp1({'win':'b','pair':'no'});
				} else {
					RenderHistoryDetailTemp1({'win':'p','pair':'no'});
				}
				c_point_v = c_point_v + 1;
			}				
		} else {
			return;
		}
	}
	
	//중국점 2번 그려주기.
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw_2 == 1) {			
		if(c_point[c_point_v] > 1) {
			c_point_cnt_2 = c_point[c_point_v - 2] - c_point[c_point_v];
			
			if (c_point_cnt_2 == -1) {
				RenderHistoryDetailTemp2({'win':'p','pair':'no'});
			} else {
				RenderHistoryDetailTemp2({'win':'b','pair':'no'});
			}
		} else {
			c_point_cnt_2 = c_point[c_point_v - 1] - c_point[c_point_v - 3];
			
			if (c_point_cnt_2 == 0) {
				RenderHistoryDetailTemp2({'win':'b','pair':'no'});
			} else {
				RenderHistoryDetailTemp2({'win':'p','pair':'no'});
			}
		}
	} else {
		//sw = 0
		if (c_point[2] > 0) {
			c_point_sw_2 = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt_2 = c_point[c_point_v-2] - i;
				
				if (c_point_cnt_2 > 0 || c_point_cnt_2 > -1) {
					RenderHistoryDetailTemp2({'win':'b','pair':'no'});
				} else {
					RenderHistoryDetailTemp2({'win':'p','pair':'no'});
				}
				
			}				
		} else {
			return;
		}
	}
	
	
	//중국점 3번 그려주기.
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw_3 == 1) {			
		if(c_point[c_point_v] > 1) {
			c_point_cnt_3 = c_point[c_point_v - 3] - c_point[c_point_v];
			
			if (c_point_cnt_3 == -1) {
				RenderHistoryDetailTemp3({'win':'p','pair':'no'});
			} else {
				RenderHistoryDetailTemp3({'win':'b','pair':'no'});
			}
		} else {
			c_point_cnt_3 = c_point[c_point_v - 1] - c_point[c_point_v - 4];
			
			if (c_point_cnt_3 == 0) {
				RenderHistoryDetailTemp3({'win':'b','pair':'no'});
			} else {
				RenderHistoryDetailTemp3({'win':'p','pair':'no'});
			}
		}
	} else {
		//sw = 0
		if (c_point[3] > 0) {
			c_point_sw_3 = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt_3 = c_point[c_point_v-2] - i;
				
				if (c_point_cnt_3 > 0 || c_point_cnt_3 > -1) {
					RenderHistoryDetailTemp3({'win':'b','pair':'no'});
				} else {
					RenderHistoryDetailTemp3({'win':'p','pair':'no'});
				}
			}				
		} else {
			return;
		}
	}
	
}

//질문(가상데이타 중국점용)
function RenderHistoryDetailTemp1 (result) {
	//{"win":"p","pair":"","big":9}
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data_1;
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win_1 == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		// detail history image
	var pair_str = (result.pair != '') ? '_' + result.pair : '';
	//var detail_img = result.win + '_2.png';
	var detail_img = 'ch_1_' + result.win.toLowerCase() + pair_str + '.png?v=300';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-1-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');

	//$("#history-detail-1-" + x + "-" + y).html('<img src="' + parent.static_domain + 'images/game/baccara_test/new_result/' + detail_img + '" align="absmiddle" alt="result_img" width="9" height="9" />');
		var animation = setInterval(function() {
			if ($("#history-detail-1-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-1-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-1-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-1-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_1_' + result.win.toLowerCase() + pair_str + '.png?v=300';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-1-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-1-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-1-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-1-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-1-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
}

function RenderHistoryDetailTemp2 (result) {
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data_2;
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win_2 == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = result.win + '_1.png';

		$("#history-detail-2-" + x + "-" + y).html('<img src="' +parent.static_domain + 'images/game/baccara_test/new_result/' + detail_img + '" align="absmiddle" alt="result_img" width="9" height="9" />');
		var animation = setInterval(function() {
			if ($("#history-detail-2-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-2-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-2-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-2-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			x = pre_data.x - (temp_history_plus_2 - 1);
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_2_' + result.win.toLowerCase() + pair_str + '.png?v=300';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-2-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-2-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-2-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-2-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-2-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
}

function RenderHistoryDetailTemp3 (result) {
	
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data_3;
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win_3 == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_3_' + result.win.toLowerCase() + pair_str + '.png?v=300';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-3-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-3-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-3-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-3-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-3-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			x = pre_data.x - (temp_history_plus_3 - 1);
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_3_' + result.win.toLowerCase() + pair_str + '.png?v=300';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-3-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-3-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-3-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-3-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-3-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
}