var clock = null;
var history_record = [];
var history_record_detail = [];
var history_simple_idx = -1;
var history_simple_detail_idx = -1;
var history_pre_data = null;
var history_last_win = '';
var history_last_x = -1;
var history_detail_arr = [];
var temp_history_plus = 0;
var temp_history_plus_1 = 0;
var temp_history_plus_2 = 0;
var temp_history_plus_3 = 0;
var history_record_detail_1 = [];
var history_pre_data_1 = null;
var history_last_win_1 = '';
var history_last_x_1 = -1;
var history_detail_arr_1 = [];
var real_count= 1;
var history_record_detail_2 = [];
var history_pre_data_2 = null;
var history_last_win_2 = '';
var history_last_x_2 = -1;
var history_detail_arr_2 = [];
var real_count= 1;
var history_record_detail_3 = [];
var history_pre_data_3 = null;
var history_last_win_3 = '';
var history_last_x_3 = -1;
var history_detail_arr_3 = [];
var real_count= 1;
var c_point=[];
var j_point=[];
var c_point_sw = 0;
var c_point_count = 0;
var c_point_result = 0;
var c_point_cnt = 0;
var c_point_v = -1;
var last_result = null;
var simple_x = 0;
var simple_y = 0;
var history_counts_pixel = 0;
var history_counts = 1;
var shoe_change_check = 0;
var shoe_change_check_1 = 0;
var shoe_change_check_2 = 0;
var shoe_change_check_3 = 0;
var c_point_sw_1 = null;
var c_point_sw_2 = null;
var c_point_sw_3 = null;
var shoebox = 0;
var scroll_simple = null;
var scroll_detail = null;
var scroll_detail_1 = null;
var scroll_detail_2 = null;
var scroll_detail_3 = null;
var selected_chip = 0;
var win_label = {'P' : '<span style="color:#69b7ff">플레이어</span>','B' : '<span style="color:#ff5251">뱅커</span>','T' : '<span style="color:#99ff8f">타이</span>'}
var first_mesage_checker = false;
var first_notification_checker = false;
var socket = null;
var notification_message_interval = null;

var random_nickname = ['빠꼼이', '외길뱅커', '토사장', '나와바리', '청주김실장', '부산총잡이','석양이진다','너프해보시지', '영운은죽는다'];
var random_money = [50000,100000,150000,500000,750000,1000000,300000,400000,600000,2000000,14000,10000,5000];
var status = false;
var sound_checker = true;
var all_sound_checker = {'shoechange' : false}
var card_sound_checker ={'player1' : true,'banker1' : true,'player2' : true,'banker2' : true,'player3' : true,'banker3' : true}

var betting_location_label =['','플레이어', '뱅커','타이','플레이어페어','뱅커페어'];
var betting_money_arr = [0,0,0,0,0,0];
var betting_money_arr_confirm = [0,0,0,0,0,0];
var betting_confirm = false;

var c_point=[];
var j_point=[];
var c_point_sw = 0;
var c_point_count = 0;
var c_point_result = 0;
var c_point_cnt = 0;
var c_point_v = -1;
var last_result = null;

var ch_pre_checker = false;
var btn_chip_confirm_checker = false;
/******************************************************************
 * HISTORY 
 *****************************************************************/
function MakeHistoryDetailMap() {
	if(typeof(history_detail_arr) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_1) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_1.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_1 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_1.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_2) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_2.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_2 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_2.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_3) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_3.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_3 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_3.push([{},{},{},{},{},{}]) 
		}
	}
}

function DrawHistoryBackground() {
	var tbl = document.createElement("table");
	tbl.id = "history_simple_record";
	tbl.summary = "history simple record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-simple").append(tbl);
	
	for (var i = 0; i < 6; i++) {
		var tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (var j = 0; j < 310; j++) {
			var td = document.createElement("td");
			td.id = "history-simple-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	tbl.className = "history_record";

	tbl = document.createElement("table");
	tbl.id = "history_detail_record";
	tbl.summary = "history detail record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-detail").append(tbl);

	for (i = 0; i < 6; i++) {
		tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (j = 0; j < 310; j++) {
			td = document.createElement("td");
			td.id = "history-detail-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	
	tbl = document.createElement("table");
	tbl.id = "history_simple_detail_record";
	tbl.summary = "history simple detail record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-simple-detail").append(tbl);
	
	for (var i = 0; i < 3; i++) {
		var tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (var j = 0; j < 310; j++) {
			var td = document.createElement("td");
			td.id = "history-simple-detail-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	tbl.className = "history_simple_detail";
	
}

function RenderHistorySimple(result) {
	if (result.pair == 'N') {
		result.pair = '';
	}
	
	if (history_simple_idx >= 510) {
		$("#history-simple td *").remove();
		history_simple_idx = -1;
	}
	
	history_simple_idx++;
	if (history_simple_idx > 510) {
		history_simple_idx = (history_simple_idx % 90); 
	}
	

	var turn_pos = history_simple_idx;				

	var y = Math.floor(turn_pos / 6);		 
	
	if (turn_pos < 6) { 
		var x = turn_pos;
	} else {
		var x = turn_pos % 6;
	}

	var simple_img = 'simple_'+result.win.toLowerCase() + '.png?v=600';		
	
	$("#history-simple-" + y + "-" + x).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+simple_img+'\')"></div>');
	
	simple_x = x;
	simple_y = y
}
function RenderHistoryDetailSimple(result) {
	if (result.pair == 'N') {
		result.pair = '';
	}
	
	if (history_simple_detail_idx >= 510) {
		$("#history-simple-detail td *").remove();
		history_simple_idx = -1;
	}
	
	history_simple_detail_idx++;
	if (history_simple_detail_idx > 510) {
		history_simple_detail_idx = (history_simple_detail_idx % 90); 
	}
	

	var turn_pos = history_simple_detail_idx;				

	var y = Math.floor(turn_pos / 3);		 
	
	if (turn_pos < 3) { 
		var x = turn_pos;
	} else {
		var x = turn_pos % 3;
	}

	var simple_img = 'simple_'+result.win.toLowerCase() + '.png?v=600';		
	
	$("#history-simple-detail-" + y + "-" + x).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+simple_img+'\')"></div>');
	
	simple_x = x;
	simple_y = y
}

function RenderHistoryDetail(result) {
	if (shoe_change_check === true){
		var x = 0;
		var y = 0;		
		history_pre_data = null;
		shoe_change_check = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data == null) {
		x = 0;
		y = 0;			
		history_last_x = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data.win || history_last_win == result.win) {
			var nx = history_pre_data.x;
			var ny = history_pre_data.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny - 1;
				temp_history_plus++
			} else {
				if (history_last_x != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus = 0;
				} else {
					var below_obj = history_detail_arr[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
					}
					temp_history_plus = 0;						
				}
			}
		} else {
			x = history_last_x + 1;
			history_last_x = x;
			y = 0;
			temp_history_plus = 0;
		}
	}

	//ClearHistory
	// if (x > 57) {
	// 	$("#history_detail td *").remove();
	// 	x = 0;
	// 	y = 0;
	// 	history_last_x = 0;
	// 	make_history_detail_map();
	// }
	
	history_pre_data = {
		'x' : x, 
		'y' : y, 
		'win' : result.win, 
		'pair' : result.pair
	};
	
	history_detail_arr[x][y] = history_pre_data;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=190';
	
	if (result.win != 'T') {
		history_last_win = result.win;
	}
	
	$("#history-detail-" + x + "-" + y).html('<div class="history-icon myb-'+result.turn+'" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')">'+result.number+'</div>');
}

function RenderHistoryDetail1(result) {
	//return;
	if (shoe_change_check_1 === true){
		var x = 0;
		var y = 0;		
		history_pre_data_1 = null;
		
		shoe_change_check_1 = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data_1 == null) {
		x = 0;
		y = 0;			
		history_last_x_1 = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data_1.win) {
			var nx = history_pre_data_1.x;
			var ny = history_pre_data_1.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny-1;
				temp_history_plus_1++
			} else {
				if (history_last_x_1 != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus_1 = 0;
				} else {
					var below_obj = history_detail_arr_1[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
						temp_history_plus_1 = 0;
					}						
				}
			}
		} else {
			x = history_last_x_1 + 1;
			history_last_x_1 = x;
			y = 0;
			temp_history_plus_1 = 0;
		}
	}
	
	history_pre_data_1 = {
		'x':x, 
		'y':y, 
		'win':result.win, 
		'pair' : result.pair
	};			
	
	history_detail_arr_1[x][y] = history_pre_data_1;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'ch_1_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-1-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
}

function RenderHistoryDetail2(result) {
	//return;
	if (shoe_change_check_2 === true){
		var x = 0;
		var y = 0;		
		history_pre_data_2 = null;
		
		shoe_change_check_2 = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data_2 == null) {
		x = 0;
		y = 0;			
		history_last_x_2 = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data_2.win || (history_last_win_2 == result.win)) {
			var nx = history_pre_data_2.x;
			var ny = history_pre_data_2.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny-1;
				temp_history_plus_2++
			} else {
				if (history_last_x_2 != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus_2 = 0;
				} else {
					var below_obj = history_detail_arr_2[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
						temp_history_plus_2 = 0;
					}						
				}
			}
			
		} else {
			x = history_last_x_2 + 1;
			history_last_x_2 = x;
			y = 0;
			temp_history_plus_2 = 0;
		}
	}
	
	history_pre_data_2 = {
		'x':x, 
		'y':y, 
		'win':result.win, 
		'pair' : result.pair
	};			
	
	history_detail_arr_2[x][y] = history_pre_data_2;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'ch_2_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-2-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
}

function RenderHistoryDetail3(result) {
	//return;
	if (shoe_change_check_3 === true){
		var x = 0;
		var y = 0;		
		history_pre_data_3 = null;
		
		shoe_change_check_3 = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data_3 == null) {
		x = 0;
		y = 0;			
		history_last_x_3 = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data_3.win || (history_last_win_3 == result.win)) {
			var nx = history_pre_data_3.x;
			var ny = history_pre_data_3.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny-1;
				temp_history_plus_3++
			} else {
				if (history_last_x_3 != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus_3 = 0;
				} else {
					var below_obj = history_detail_arr_3[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
						temp_history_plus_3 = 0;
					}						
				}
			}
			
		} else {
			x = history_last_x_3 + 1;
			history_last_x_3 = x;
			y = 0;
			temp_history_plus_3 = 0;
		}
	}
	
	history_pre_data_3 = {
		'x':x, 
		'y':y, 
		'win':result.win, 
		'pair' : result.pair
	};			
	
	history_detail_arr_3[x][y] = history_pre_data_3;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'ch_3_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	$("#history-detail-3-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');

}

function ClearHistory() {
	$("#history-simple td *").remove();
	$("#history-detail td *").remove();
	$("#history-simple-detail td *").remove();
	$("#history-detail-1 td *").remove();
	$("#history-detail-2 td *").remove();
	$("#history-detail-3 td *").remove();
	history_record = [];
	history_record_detail = [];
	history_simple_idx = -1;
	history_simple_detail_idx = -1;
	history_pre_data = null;
	history_last_win = '';
	history_last_x = -1;
	history_detail_arr = [];
	temp_history_plus = 0;
	temp_history_plus_1 = 0;
	temp_history_plus_2 = 0;
	temp_history_plus_3 = 0;
	history_record_detail_1 = [];
	history_pre_data_1 = null;
	history_last_win_1 = '';
	history_last_x_1 = -1;
	history_detail_arr_1 = [];
	real_count= 1;
	history_record_detail_2 = [];
	history_pre_data_2 = null;
	history_last_win_2 = '';
	history_last_x_2 = -1;
	history_detail_arr_2 = [];
	real_count= 1;
	history_record_detail_3 = [];
	history_pre_data_3 = null;
	history_last_win_3 = '';
	history_last_x_3 = -1;
	history_detail_arr_3 = [];
	real_count= 1;
	c_point=[];
	j_point=[];
	c_point_sw = 0;
	c_point_count = 0;
	c_point_result = 0;
	c_point_cnt = 0;
	c_point_v = -1;
	last_result = null;
	simple_x = 0;
	simple_y = 0;
	history_counts_pixel = 0;
	history_counts = 1;
	shoe_change_check = 0;
	shoe_change_check_1 = 0;
	shoe_change_check_2 = 0;
	shoe_change_check_3 = 0;
	c_point_sw_1 = null;
	c_point_sw_2 = null;
	c_point_sw_3 = null;
	MakeHistoryDetailMap();
}
/******************************************************************
 * CHINESE POINT
 *****************************************************************/
function c_1() {
	var result = {"win":"B","pair":"",'tmp':'b'};
    var data = history_pre_data;
    if (data == null) { 
        return;
    }
    RenderHistorySimpleTemp(result);
	RenderHistoryDetailTemp(result);
}

function c_2() {
	var result = {"win":"P","pair":"",'tmp':'p'};
    var data = history_pre_data;
    if (data == null) { 
        return;
    }
    RenderHistorySimpleTemp(result);
	RenderHistoryDetailTemp(result);
}

//예상 히스토리 그려주기(일반 심플)
function RenderHistorySimpleTemp (result) {
    if (result.pair == 'N') {
		result.pair = '';
	}
    
    //일반 히스토리 x와 y 값을 따로 변수로 지정 받아둔다.
    if (simple_y == 5){
        var x = 0;
        var y = simple_y + 1;
    } else {
        var x = simple_x + 1;
        var y = simple_y;
    }

	var simple_img = 'simple_'+result.win.toLowerCase() + '.png?v=600';		
	
	$("#history-simple-" + y + "-" + x).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+simple_img+'\')"></div>');
    
    var animation = setInterval(function() {
		if ($("#history-simple-" + y + "-" + x + " *").css('opacity') == '1') {
			$("#history-simple-" + y + "-" + x + " *").fadeOut('slow');
		} else {
			$("#history-simple-" + y + "-" + x + " *").fadeIn('slow');
		}
	});
	setTimeout(function() {
		clearInterval(animation);
		$("#history-simple-" + y + "-" + x + " *").fadeOut();
	},1000);
}

function RenderHistoryDetailTemp (result) {
	var x;
	var y;
	
	if (result.pair == 'N') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data;
	
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=600';
		
		$("#history-detail-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			x = pre_data.x - (temp_history_plus - 1);
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=600';
		
		$("#history-detail-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		var animation = setInterval(function() {
			if ($("#history-detail-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
	
	if(result.win == 'T') {
		return;
	} else {
		if (last_result != result.win) {
			j_point = 0;
			j_point = j_point + 1;
			c_point.push(j_point);
			c_point_v = c_point_v + 1
		}

		//내용이 같은것은 아래로~
		if (last_result == result.win) {
			j_point = j_point + 1;
			c_point.pop();
			c_point.push(j_point);
		}
		last_result = result.win;
	}
	
	//중국점 1번 을 그려줍세
	if(c_point_sw == 1) {	
		if (c_point_v[c_point_v+1] > 0) {
			
			c_point_v = c_point_v + 1;
		}	
		
		if(c_point[c_point_v] > 1) {
			c_point_cnt = c_point[c_point_v - 1] - c_point[c_point_v];
			
			if (c_point_cnt == -1) {
				RenderHistoryDetailTemp1({'win':'P','pair':'N','tmp':result.tmp});
			} else {
				RenderHistoryDetailTemp1({'win':'B','pair':'N','tmp':result.tmp});
			}
		} else {
			c_point_cnt = c_point[c_point_v - 1] - c_point[c_point_v - 2];
			
			if (c_point_cnt == 0) {
				RenderHistoryDetailTemp1({'win':'B','pair':'N','tmp':result.tmp});
			} else {
				RenderHistoryDetailTemp1({'win':'P','pair':'N','tmp':result.tmp});
			}
		}
	} else {
		//sw = 0
		if (c_point[1] > 0) {
			c_point_sw = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt = c_point[c_point_v-1] - i;
				
				if (c_point_cnt > 0 || c_point_cnt > -1) {
					RenderHistoryDetailTemp1({'win':'B','pair':'N','tmp':result.tmp});
				} else {
					RenderHistoryDetailTemp1({'win':'P','pair':'N','tmp':result.tmp});
				}
				c_point_v = c_point_v + 1;
			}				
		} else {
			return;
		}
	}
	
	//중국점 2번 그려주기.
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw_2 == 1) {			
		if(c_point[c_point_v] > 1) {
			c_point_cnt_2 = c_point[c_point_v - 2] - c_point[c_point_v];
			
			if (c_point_cnt_2 == -1) {
				RenderHistoryDetailTemp2({'win':'P','pair':'N','tmp':result.tmp});
			} else {
				RenderHistoryDetailTemp2({'win':'B','pair':'N','tmp':result.tmp});
			}
		} else {
			c_point_cnt_2 = c_point[c_point_v - 1] - c_point[c_point_v - 3];
			
			if (c_point_cnt_2 == 0) {
				RenderHistoryDetailTemp2({'win':'B','pair':'N','tmp':result.tmp});
			} else {
				RenderHistoryDetailTemp2({'win':'p','pair':'N','tmp':result.tmp});
			}
		}
	} else {
		//sw = 0
		if (c_point[2] > 0) {
			c_point_sw_2 = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt_2 = c_point[c_point_v-2] - i;
				
				if (c_point_cnt_2 > 0 || c_point_cnt_2 > -1) {
					RenderHistoryDetailTemp2({'win':'B','pair':'N','tmp':result.tmp});
				} else {
					RenderHistoryDetailTemp2({'win':'P','pair':'N','tmp':result.tmp});
				}
				
			}				
		} else {
			return;
		}
	}
	
	
	//중국점 3번 그려주기.
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw_3 == 1) {			
		if(c_point[c_point_v] > 1) {
			c_point_cnt_3 = c_point[c_point_v - 3] - c_point[c_point_v];
			
			if (c_point_cnt_3 == -1) {
				RenderHistoryDetailTemp3({'win':'P','pair':'N','tmp':result.tmp});
			} else {
				RenderHistoryDetailTemp3({'win':'B','pair':'N','tmp':result.tmp});
			}
		} else {
			c_point_cnt_3 = c_point[c_point_v - 1] - c_point[c_point_v - 4];
			
			if (c_point_cnt_3 == 0) {
				RenderHistoryDetailTemp3({'win':'B','pair':'N','tmp':result.tmp});
			} else {
				RenderHistoryDetailTemp3({'win':'P','pair':'N','tmp':result.tmp});
			}
		}
	} else {
		//sw = 0
		if (c_point[3] > 0) {
			c_point_sw_3 = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt_3 = c_point[c_point_v-2] - i;
				
				if (c_point_cnt_3 > 0 || c_point_cnt_3 > -1) {
					RenderHistoryDetailTemp3({'win':'B','pair':'N','tmp':result.tmp});
				} else {
					RenderHistoryDetailTemp3({'win':'P','pair':'N','tmp':result.tmp});
				}
			}				
		} else {
			return;
		}
	}
	
}

//질문(가상데이타 중국점용)
function RenderHistoryDetailTemp1 (result) {
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data_1;
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win_1 == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
	
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
	
		var detail_img = 'ch_1_' + result.win.toLowerCase() + '.png?v=600';
	
		$("#history-detail-1-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');

		$('.ch_1_'+result.tmp).attr('src',CONFIG.ASSETS+'/img/history/'+detail_img);
		
		

		var animation = setInterval(function() {
			if ($("#history-detail-1-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-1-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-1-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-1-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_1_' + result.win.toLowerCase() + '.png?v=600';
		
		$("#history-detail-1-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		$('.ch_1_'+result.tmp).attr('src',CONFIG.ASSETS+'/img/history/'+detail_img);
		
		
		
		var animation = setInterval(function() {
			if ($("#history-detail-1-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-1-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-1-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-1-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
}

function RenderHistoryDetailTemp2 (result) {
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data_2;
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win_2 == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_2_' + result.win.toLowerCase() + '.png?v=600';
	
		$("#history-detail-2-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		$('.ch_2_'+result.tmp).attr('src',CONFIG.ASSETS+'/img/history/'+detail_img);
		
		
		
		var animation = setInterval(function() {
			if ($("#history-detail-2-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-2-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-2-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-2-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			x = pre_data.x - (temp_history_plus_2 - 1);
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_2_' + result.win.toLowerCase() + '.png?v=600';
	
		$("#history-detail-2-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		$('.ch_2_'+result.tmp).attr('src',CONFIG.ASSETS+'/img/history/'+detail_img);
		

		
		var animation = setInterval(function() {
			if ($("#history-detail-2-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-2-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-2-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-2-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
}

function RenderHistoryDetailTemp3 (result) {
	var x;
	var y;
	
	if (result.pair == 'no') {
		result.pair = '';
	}
	
	//디테일 히스토리
	var pre_data = history_pre_data_3;
	
	if (pre_data.win == result.win || result.win == 'T' || (history_last_win_3 == result.win)) {
		x = pre_data.x + 0
		y = pre_data.y + 1
		
		if (y > 5) {
			x = pre_data.x + 1
			y = pre_data.y
		}
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_3_' + result.win.toLowerCase() + '.png?v=600';
	
		$("#history-detail-3-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		$('.ch_3_'+result.tmp).attr('src',CONFIG.ASSETS+'/img/history/'+detail_img);
		
		
		
		var animation = setInterval(function() {
			if ($("#history-detail-3-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-3-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-3-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-3-" + x + "-" + y + " *").fadeOut();
		},1000);
	} else {
		if (pre_data.y == 5) {
			x = pre_data.x - (temp_history_plus_3 - 1);
			y = 0 ;
		} else {
			x = pre_data.x + 1
			y = 0;
		}
		
		
		var pair_str = (result.pair != '') ? '_' + result.pair : '';
		var detail_img = 'ch_3_' + result.win.toLowerCase() + '.png?v=600';
	
		$("#history-detail-3-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
		$('.ch_3_'+result.tmp).attr('src',CONFIG.ASSETS+'/img/history/'+detail_img);
		
		
		
		var animation = setInterval(function() {
			if ($("#history-detail-3-" + x + "-" + y + " *").css('opacity') == '1') {
				$("#history-detail-3-" + x + "-" + y + " *").fadeOut('slow');
			} else {
				$("#history-detail-3-" + x + "-" + y + " *").fadeIn('slow');
			}
		});
		setTimeout(function() {
			clearInterval(animation);
			$("#history-detail-3-" + x + "-" + y + " *").fadeOut();
		},1000);
	}
}

/******************************************************************
 * CHAT
 *****************************************************************/
function ChatInit() {
	// LoadScript(CONFIG.ASSETS+'/js/socket.io.js', function () {
	// 	socket = io.connect('https://socket.8008.company', {
	// 		"transports":['websocket'], 
	// 		"upgrade" : false,
	// 		"force new connection" : true,
	// 		"reconnection" : false
	// 	});
	// });
}
function ChatMessage(message, nickname) {
	$('.chat-list-box ul').append('<li><span style="color:#4cfd3b">['+nickname+']</span> : '+message+'</li>');
	ChatScroll();
}

function ChatScroll() {
	var objDiv = document.getElementById("chat-list-box");
	objDiv.scrollTop = objDiv.scrollHeight;
}

function NotificationMessage(message) {
	$('.message-box').text(message);
	$('.message-box').fadeIn()
	if(notification_message_interval != null) {
		clearInterval(notification_message_interval);
	}

	notification_message_interval = setInterval(function () {
		$('.message-box').fadeOut();
	},3000)
	setTimeout(function () {
		clearInterval(notification_message_interval);
	},4000)
	
}

function FullScreen() {
	$('body').css('zoom', '110%');
}
/******************************************************************
 * COUNTDOWN
 *****************************************************************/
function ClockLoad() {
	LoadScript(CONFIG.ASSETS+'/js/flipclock.min.js', function () {
		clock = new FlipClock($('#count-down'), 40, {clockFace: 'Counter'});
	},false)
}

function CountDown(time) {
	if(time > 0) {
		$('#count-down').css('opacity', 1);
		if(btn_chip_confirm_checker === false) {
			
			$('.chip-bw').slideUp({complete: function(){
				$(this).hide();
				$('.chip-box').show({complete: function(){
				$(this).slideDown();
				}});
			}});
		}
		clock.setCounter(time);
	} else {
		$('#count-down').css('opacity', 0);
		$('.chip-box').slideUp({complete: function(){
			$(this).hide();
			$('.chip-bw').show({complete: function(){
				$(this).slideDown();
			}});
		}});
		$('.bet-confirm-box').hide();
		clock.setCounter(40);
	}
}

/************************************************************
 * GetMyMoney
 ***********************************************************/
function GetMyMoney() {
	PostJson(CONFIG.AJAX+'?mode=get_my_money', {}, function (returnData) {
		if(returnData.ErrorCode == 0) {
			$('#my-money').text(AddComma(returnData.Data.Money));
			CONFIG.MEMBER.MONEY = returnData.Data.Money;
		} else {
			return 0;
		}
	})
}

/******************************************************************
 * GET GAME HISTORY
 *****************************************************************/
function GetHistory() {
	PostJson(CONFIG.AJAX+'?mode=history',{token:'abcd'}, function (returnData) {
		if(returnData.ErrorCode != 0) {
			alert('데이터 오류로 인해 새로고침이 됩니다.');
			location.reload();
		} else {
			var tempNumber;
			var records = returnData.Data;

			for(var i in records) {
				switch(records[i]['result']){
					case "P":
						tempNumber = records[i]['player_last_number'];
						break;
					case "B":
						tempNumber = records[i]['banker_last_number'];
						break;
					default:
						tempNumber = 0;
						break;
				}
				if(records[i]['status'] != '-') {
					
					var json_data = {
						win : records[i]['result'], 
						pair : records[i]['pair'],
						pair_player : records[i]['pair_player'],
						pair_banker : records[i]['pair_banker'],
						turn : records[i]['turn'],
						number : tempNumber
					}

					$('.count-'+records[i]['result'].toLowerCase()).text(parseInt($('.count-'+records[i]['result'].toLowerCase()).text()) + 1)
					
					if(records[i]['pair_player'] == 'Y') {
						$('.count-pp').text(parseInt($('.count-pp').text()) + 1)
					}

					if(records[i]['pair_banker'] == 'Y') {
						$('.count-bp').text(parseInt($('.count-bp').text()) + 1)
					}
					RenderHistorySimple(json_data);
					RenderHistoryDetail(json_data);
					
					RenderHistoryDetailSimple(json_data);
				}
			}
			LoadScript(CONFIG.ASSETS+'/js/iscroll.js', function () {
				scroll_simple = new IScroll('#history-simple', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

				scroll_detail = new IScroll('#history-detail', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

			});
			
			setTimeout(function () {
				if(history_pre_data.x > 5) {
					scroll_detail.scrollTo( ( (history_pre_data.x-5) * 7) * -1, 0);	
				}
				
			},1000)
		}
	});
}

function OffBetPanel() {
	$('.win-p').css("background-image",$('.win-p').css("background-image").replace(/\_on\.png/, '\_off\.png'));
	$('.win-b').css("background-image",$('.win-b').css("background-image").replace(/\_on\.png/, '\_off\.png'));
	$('.win-t').css("background-image",$('.win-t').css("background-image").replace(/\_on\.png/, '\_off\.png'));
}

function SoundPlay (sound) {
	if(sound_checker === false) {
		return;
	}
	if ($("#SOUND-"+sound).length == 0) {
		$("body").append(
			$("<audio>").attr("id","SOUND-"+sound)
				.append($("<source>")
				.attr("src",CONFIG.ASSETS+"/sound/"+sound+".mp3?v=200")
				.attr("type","audio/mpeg")
			)
		);
	}
	$("#SOUND-"+sound).get(0).play();
}

/*********************************************************************************************************
 * EVENT
 ********************************************************************************************************/
$('.btn-bet').hover(function() {
	$(this).css("background-image",$(this).css("background-image").replace(/\_off\.png/, '\_on\.png'));
}, function() { 
	if($(this).find(".bet-money-chip-bet").length <= 0){
		$(this).css("background-image",$(this).css("background-image").replace(/\_on\.png/, '\_off\.png'));
	}
});

$('.second-btn-box').hover(function() {
	$(this).attr('src',$(this).attr("src").replace(/\_off\.png/, '\_on\.png'));
}, function() { 
	$(this).attr('src',$(this).attr("src").replace(/\_on\.png/, '\_off\.png'));
});

var log_toggle = false;
var log_toggle2 = false;

$('.history-show').on('click', function () {
	log_toggle = true;
	log_toggle2 = true;
	$('.official-list').hide();
	$('.chat-list-box').hide();
	$('.history-box').toggle();
})

$('.toggle-button').on('click', function () {
	if(log_toggle == false) {
		log_toggle = true;
		$('.chat-list-box').hide();
	} else {
		log_toggle = false;
		$('.official-list').hide();
		$('.history-box').hide();
		$('.chat-list-box').show();
	}
})
$('.official-show').on('click', function () {
	if(log_toggle2 == false) {
		log_toggle2 = true;
		$('.official-list').hide();
	} else {
		log_toggle2 = false;
		log_toggle = true;
		$('.history-box').hide();
		$('.official-list').show();
	}
})

$('.btn-bet').on('click', function () {
	var options = {
		useEasing: true, 
		useGrouping: true, 
		separator: ',', 
		decimal: '.', 
		};
	var before_money = 0;
	SoundPlay('chip_on');
	var label = $(this).data('label');
	var location = $(this).data('location');
	if(selected_chip <= 0) {
		ChatMessage('<span style="color:#ff5251">칩을 선택해주세요</span>', '시스템');
	} else {
		if(CONFIG.MEMBER.MONEY < selected_chip) {
			ChatMessage('<span style="color:#ff5251">보유머니가 부족합니다.</span>', '시스템');
		} else {
			if( (SumArray(betting_money_arr) + selected_chip) > CONFIG.MEMBER.MONEY) {
				ChatMessage('<span style="color:#ff5251">보유머니가 부족합니다.</span>', '시스템');
			}
			else
			{
				if(location == 1 || location == 2) {
					if(CONFIG.GAME.MIN > betting_money_arr[location] + selected_chip) {
						ChatMessage(label+'위치 최소 베팅금액은 <span style="color:#ff5251">'+AddComma(CONFIG.GAME.MIN)+'원 입니다.</span>', '시스템');
					} else if(CONFIG.GAME.MAX < selected_chip || CONFIG.GAME.MAX < betting_money_arr[location] + selected_chip) {
						ChatMessage(label+'위치 최대 베팅금액은 <span style="color:#ff5251">'+AddComma(CONFIG.GAME.MAX)+'원 입니다.</span>', '시스템');
						return;
					}
				} else if (location == 3 || location == 4 || location == 5) {
					if((CONFIG.GAME.MIN /10) > betting_money_arr[location] + selected_chip) {
						ChatMessage(label+'위치 최소 베팅금액은 <span style="color:#ff5251">'+AddComma(CONFIG.GAME.MIN / 10)+'원 입니다.</span>', '시스템');
					} else if((CONFIG.GAME.MAX / 10) < selected_chip || (CONFIG.GAME.MAX /10) < betting_money_arr[location] + selected_chip) {
						ChatMessage(label+'위치 최대 베팅금액은 <span style="color:#ff5251">'+AddComma(CONFIG.GAME.MAX / 10)+'원 입니다.</span>', '시스템');
						return;
					}
				}
				before_money = betting_money_arr[location];
				betting_money_arr[location] = betting_money_arr[location] + selected_chip;
				var countup = new CountUp('bet-money-'+location, before_money, betting_money_arr[location], 0, 0.5, options);
				console.log('.bet-money-'+location);
				countup.start();
				$('.bet-money-chip-'+location).addClass("bet-money-chip-bet");
				//베팅판때기에 칩을 올린다.
				/*if($('.btn-panel-'+location+' .panel_chip_'+selected_chip_location).length <= 0) {
					// if(location == 1 || location == 2)
					// {
						$('.btn-panel-'+location).append('<div class="panel_chip panel_chip_'+selected_chip_location+'" style="bottom:100px"><img src="'+CONFIG.ASSETS+'/img/chip/panel_chip_'+selected_chip_location+'.png"></div>');		
					//}
				}
				else if($('.btn-panel-'+location+' .panel_chip_'+selected_chip_location).length > 0 && $('.btn-panel-'+location+' .panel_chip_'+selected_chip_location).length <= 10)
				{
					var bottom = parseInt($('.btn-panel-'+location+' .panel_chip_'+selected_chip_location+':last').css('bottom').replace('px',''));
					$('.btn-panel-'+location).append('<div class="panel_chip panel_chip_'+selected_chip_location+'" style="bottom:'+(bottom+8)+'px"><img src="'+CONFIG.ASSETS+'/img/chip/panel_chip_'+selected_chip_location+'.png"></div>');	

				}*/
			}	
		}
	}
});

$('.chip-btn').on('click', function () {
	SoundPlay('chip');
	$('.chip-btn-money').each(function (index,el) {
		$(el).removeClass('flicker');
	});

	$('.chip-btn').each(function (index,el) {
		$(el).attr('src', $(el).attr('src').replace(/\_on\.png/, '\_off\.png'));
		$(el).removeClass('flicker');
	});
	
	var money = $(this).data('money');
	var money_location = $(this).data('location');
	if($(this).attr('src').indexOf('_on.png') != -1) {
		$(this).attr('src', $(this).attr('src').replace(/\_on\.png/, '\_off\.png'));
		selected_chip = 0;
		$(this).removeClass('flicker');
	} else {
		$(this).attr('src', $(this).attr('src').replace(/\_off\.png/, '\_on\.png'))
		if(money != selected_chip) {
			ChatMessage(AddComma(money)+'칩 선택','시스템');
			selected_chip = money;
			selected_chip_location = money_location;
		}
		$(this).addClass('flicker');
	}
})



$('.input-box-text').on('keypress', function (event) {
	var message = ($('.input-box-text').val())
	if(event.charCode == 13 || event.keyCode == 13 || event.which == 13) {
		if(message == '' || message.length <=0 ) {
			return;
		}

		PostJson(CONFIG.AJAX+'?mode=chat', {nickname : CONFIG.MEMBER.NICKNAME, message : message}, function (returnData) {
			if(returnData.ErrorCode == 0)
			{
				ChatMessage('메세지 전송(딜레이 3초)', '비트윈');	
			} else {
				ChatMessage('한글,영어,숫자만 가능.', '비트윈');	
			}
			$('.input-box-text').val('')
			ChatScroll();
		});
	}
})

$('.send-button').on('click', function () {
	var message = ($('.input-box-text').val())
	if(message == '' || message.length <=0 ) {return;}
	PostJson(CONFIG.AJAX+'?mode=chat', {nickname : CONFIG.MEMBER.NICKNAME, message : message}, function (returnData) {
		if(returnData.ErrorCode == 0)
		{
			ChatMessage('메세지 전송(딜레이 3초)', '비트윈');	
		} else {
			ChatMessage('한글,영어,숫자만 가능.', '비트윈');	
		}
		$('.input-box-text').val('')
		ChatScroll();
	});
})

$('.btn-chip-reset').on('click', function () {
	betting_money_arr = [0,0,0,0,0,0];
	$('.bet-money-chip-bet').hide({complete: function(){
		$(this).parent().css("background-image",$(this).parent().css("background-image").replace(/\_on\.png/, '\_off\.png'));
		$(this).removeClass("bet-money-chip-bet");
	}});
	ChatMessage('베팅초기화를 하였습니다.', '시스템');
	$('.panel_chip').remove();
	$('.bet-money').text(0);
})

$('.btn-chip-confirm').on('click', function () {
	$('.bet-money-chip-bet').hide({complete: function(){
		$(this).parent().css("background-image",$(this).parent().css("background-image").replace(/\_on\.png/, '\_off\.png'));
	}});
	$('.bet-money-chip-bet').show({complete: function(){
		
		$(this).parent().css("background-image",$(this).parent().css("background-image").replace(/\_off\.png/, '\_on\.png'));
	}
	});
	PostJson(CONFIG.AJAX+'?mode=betting', {bettings : betting_money_arr}, function (returnData) {
		if(returnData.ErrorCode != 0)
		{
			// betting_money_arr = [0,0,0,0,0,0];
			// $('.bet-money').text(0);
			ChatMessage('이미 베팅한 내역이있거나 실패 하였습니다.', '시스템');
		}
		else
		{
			_.each(betting_money_arr, function (row,index) {
				if(row > 0) {
					ChatMessage(AddComma(row)+'원 베팅', betting_location_label[index]);	
				}
			});
			betting_confirm = true;
			_.each(betting_money_arr_confirm, function (row,index) {
				betting_money_arr_confirm[index] =betting_money_arr_confirm[index] + betting_money_arr[index];
				
			});

			betting_money_arr = [0,0,0,0,0,0];
			_.each(betting_money_arr_confirm, function (row,index) {
				if(row > 0) {
					$('.bet-money-'+index).text( AddComma(betting_money_arr_confirm[index]));
				}
			});
			$('.bet-confirm-box').fadeOut();
			btn_chip_confirm_checker = false;
		}
	});
});


var PlayLoop = setInterval(function () {
	$('.win_flicker').removeClass('flicker_three').removeClass('win_flicker');
	PostJson(CONFIG.AJAX+'?mode=play',{}, function (returnData) {
		if(returnData.ErrorCode != 0) {
			alert('데이터 오류로 인해 새로고침이 됩니다.');
			location.reload();
		} else {
			GetMyMoney();
			var data = returnData.Data;
			if(data == null) {
				return;
			}
			
			status = data['status'];

			if(data['shoebox_change'] == 'Y')
			{
				NotificationMessage('슈 체인지. 약 5분동안 진행.');
				if(all_sound_checker['shoechange'] === false)
				{
					all_sound_checker['shoechange'] = true;
					SoundPlay('shoechange');	
				}
				else
				{
					data['shoebox_change'] = false;
				}
				
			}

			if(typeof(data['shoebox']) == 'undefined') {
				return;
			}

			if(shoebox == 0) {
				shoebox = data['shoebox'];
			}

			if(shoebox != data['shoebox']) {
				ClearHistory();
				shoebox = data['shoebox'];
				return;
			}

			//COUNT
			if(data['count'] > 0) {
				if(first_mesage_checker === false) {
					first_mesage_checker = true;
					//ChatMessage(data['turn'] +'회차 베팅시작 되었습니다.', '시스템');
					OffBetPanel();
					betting_money_arr_confirm = [0,0,0,0,0,0];
					betting_money_arr = [0,0,0,0,0,0];
					$('.bet-money').text(0);
					$('.panel_chip').remove();
				}

				if(data['count'] <= 10 && data['count'] >= 1) {
					SoundPlay(data['count']);
				}

				if(first_notification_checker === false) {
					first_notification_checker = true;
					ChatMessage(data['turn']+'회 베팅시작', '비트윈')
					NotificationMessage(data['turn'] +'회차 베팅시작 되었습니다.');
					SoundPlay('betting_start');
					card_sound_checker = {'player1' : false,'banker1' : false,'player2' : false,'banker2' : false,'player3' : false,'banker3' : false}
				}
				CountDown(data['count'])	
				$('.card-player-1').removeClass('card-player-1-show')
				$('.card-player-2').removeClass('card-player-2-show')
				$('.card-player-3').removeClass('card-player-3-show')
				$('.card-banker-1').removeClass('card-banker-1-show')
				$('.card-banker-2').removeClass('card-banker-2-show')
				$('.card-banker-3').removeClass('card-banker-3-show')
				$('.card-box-blind').fadeOut()
				$('.card-box').fadeOut()
				$('.win-box').fadeOut();
			} else {
				if(first_mesage_checker === true) {
					first_mesage_checker = false;
					//ChatMessage(data['turn'] +'회차 베팅이 종료 되었습니다.', '시스템')
					NotificationMessage(data['turn'] +'회차 베팅종료 되었습니다.');
					SoundPlay('betting_end');	
					if(betting_confirm === false)
					{
						ChatMessage('베팅 미확인으로 인해 베팅 초기화', '시스템')
						betting_money_arr_confirm = [0,0,0,0,0,0];
						betting_money_arr = [0,0,0,0,0,0];
						$('.panel_chip').remove();
					}
				}
				CountDown(0);
				


				$('.card_players').fadeIn();
				$('.card_bankers').fadeIn();
				$('.card-box-blind').fadeIn()
				$('.card-box').fadeIn()
			}
			
			if(data['player_1_card'] != '-') {
				$('.card-player-1').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['player_1_card'].toLowerCase()+'.png")');
				$('.card-player-1').addClass('card-player-1-show');
				if(card_sound_checker.player1 === false) {
					card_sound_checker.player1 = true
					SoundPlay('player')
					setTimeout(function () {
						SoundPlay('card_deal');
					},2000)
				}
			}

			if(data['player_2_card'] != '-') {
				$('.card-player-2').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['player_2_card'].toLowerCase()+'.png")');
				$('.card-player-2').addClass('card-player-2-show');
				if(card_sound_checker.player2 === false) {
					card_sound_checker.player2 = true
					SoundPlay('player')
					setTimeout(function () {
						SoundPlay('card_deal');	
					},2000)
				}
			}

			if(data['player_3_card'] != '-') {
				$('.card-player-3').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['player_3_card'].toLowerCase()+'.png")');
				$('.card-player-3').addClass('card-player-3-show');
				if(card_sound_checker.player3 === false) {
					card_sound_checker.player3 = true
					SoundPlay('player')
					setTimeout(function () {
						SoundPlay('card_deal');	
					},2000)
				}
			}

			if(data['banker_1_card'] != '-') {
				$('.card-banker-1').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['banker_1_card'].toLowerCase()+'.png")');
				$('.card-banker-1').addClass('card-banker-1-show');
				if(card_sound_checker.banker1 === false) {
					card_sound_checker.banker1 = true
					SoundPlay('banker')
					setTimeout(function () {
						SoundPlay('card_deal');	
					},2000)
				}
			}

			if(data['banker_2_card'] != '-') {
				$('.card-banker-2').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['banker_2_card'].toLowerCase()+'.png")');
				$('.card-banker-2').addClass('card-banker-2-show');
				if(card_sound_checker.banker2 === false) {
					card_sound_checker.banker2 = true
					SoundPlay('banker')
					setTimeout(function () {
						SoundPlay('card_deal');	
					},2000)
				}
			}

			if(data['banker_3_card'] != '-') {
				$('.card-banker-3').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['banker_3_card'].toLowerCase()+'.png")');
				$('.card-banker-3').addClass('card-banker-3-show');
				if(card_sound_checker.banker3 === false) {
					card_sound_checker.banker3 = true
					SoundPlay('banker')
					setTimeout(function () {
						SoundPlay('card_deal');	
					},2000)
				}
			}
			setTimeout(function () {
				$('.player-sum').text(data['player_last_number']);
				$('.banker-sum').text(data['banker_last_number']);
			},1000)
			if(data['status'] == 'COMPLETED' && data['result'] != '-') {
				$('.bet-money-chip-bet').hide({complete: function(){
					$(this).parent().css("background-image",$(this).parent().css("background-image").replace(/\_on\.png/, '\_off\.png'));
					$(this).removeClass("bet-money-chip-bet");
				}});
				$('.win-box img').attr('src', CONFIG.ASSETS+'/img/win/winner_'+data['result'].toLowerCase()+'.png?v=600');
				$('.win-box').fadeIn();
				if(data['shoebox_change'] == 'N')
				{
					
					$('.win-'+data['result'].toLowerCase()).css("background-image",$('.win-'+data['result'].toLowerCase()).css("background-image").replace(/\_off\.png/, '\_on\.png'));
					
					$('.win-'+data['result'].toLowerCase()).addClass('flicker_three').addClass('win_flicker');
				}
				else
				{
					
					$('.win-box').hide();
					$('.card-box-blind').fadeOut()
					$('.card-box').fadeOut()
				}
				var tempNumber;
				switch(data['result']){
					case "P":
						tempNumber = data['player_last_number'];
						break;
					case "B":
						tempNumber = data['banker_last_number'];
						break;
					default:
						tempNumber = 0;
						break;
				}
				if($('.myb-'+data['turn']).length <= 0) {
					var json_data = {
						win : data['result'], 
						pair : 'N',
						turn : data['turn'],
						number: tempNumber
					}

					RenderHistorySimple(json_data);
					RenderHistoryDetail(json_data);
					RenderHistoryDetailSimple(json_data);
					ChatMessage(data['turn']+'회 '+win_label[data['result']]+' 승리', '비트윈');
					SoundPlay('winner_'+data['result'].toLowerCase());
					setTimeout(function () {
						if(history_pre_data.x > 5) {
							scroll_detail.scrollTo( ( (history_pre_data.x-5) * 7) * -1, 0);	
						}

						
					},1000);

					first_notification_checker = false;
				}
			}
		}
	})
},1000)

ClockLoad();
MakeHistoryDetailMap()
DrawHistoryBackground()
GetHistory();
ChatScroll();

setTimeout(function () {
	$('.toggle-button').click();
	$('.history-show').click();
},2000)
// var canvas = document.getElementById('videoCanvas');
// var ctx = canvas.getContext('2d');
// ctx.fillStyle = '#444';
// ctx.fillText('Stream loading...', canvas.width/2-30, canvas.height/3);
// //var client = new WebSocket( 'wss://13.113.106.201:9085/' );
// var client = new WebSocket( 'wss://video.8008.company/' );
// var player = new jsmpeg(client, {canvas:canvas});