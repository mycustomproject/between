var clock = null;
var history_record = [];
var history_record_detail = [];
var history_simple_idx = -1;
var history_pre_data = null;
var history_last_win = '';
var history_last_x = -1;
var history_detail_arr = [];
var temp_history_plus = 0;
var temp_history_plus_1 = 0;
var temp_history_plus_2 = 0;
var temp_history_plus_3 = 0;

var history_record_detail_1 = [];
var history_pre_data_1 = null;
var history_last_win_1 = '';
var history_last_x_1 = -1;
var history_detail_arr_1 = [];
var real_count= 1;

var history_record_detail_2 = [];
var history_pre_data_2 = null;
var history_last_win_2 = '';
var history_last_x_2 = -1;
var history_detail_arr_2 = [];
var real_count= 1;

var history_record_detail_3 = [];
var history_pre_data_3 = null;
var history_last_win_3 = '';
var history_last_x_3 = -1;
var history_detail_arr_3 = [];
var real_count= 1;

var c_point=[];
var j_point=[];
var c_point_sw = 0;
var c_point_count = 0;
var c_point_result = 0;
var c_point_cnt = 0;
var c_point_v = -1;
var last_result = null;
var simple_x = 0;
var simple_y = 0;
var history_counts_pixel = 0;
var history_counts = 1;
var shoe_change_check = 0;
var shoe_change_check_1 = 0;
var shoe_change_check_2 = 0;
var shoe_change_check_3 = 0;
var c_point_sw_1 = null;
var c_point_sw_2 = null;
var c_point_sw_3 = null;
var shoebox = 0;
var scroll_simple = null;
var scroll_detail = null;
var scroll_detail_1 = null;
var scroll_detail_2 = null;
var scroll_detail_3 = null;
var selected_chip = 0;

var first_notification_checker = false;
var first_mesage_checker = false;
var btn_chip_confirm_checker = false;

var sound_checker = true;

var notification_message_interval = null;
var turn = 0;

var special_rate = false;
var special_loc = false;
var special_rates = [12.5,11.5,5.8,3.7,2.87,2.15,1.93,1.62,1.42,1.25,1.15,1.93,1.93,3.7,3.7,3.7,3.7,3.7,3.7,3.7,3.7,2.7,2.7,2.7,1.93,1.93];


var betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var betting_money_arr_confirm = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var betting_location_label = ['사이잡기x12.5','사이잡기x11.5','사이잡기x5.8','사이잡기x3.7','사이잡기x2.87','사이잡기x2.15','사이잡기x1.93','사이잡기x1.62','사이잡기x1.42','사이잡기x1.25','사이잡기x1.15','홀','짝','스페이드','다이아몬드','하트','크로바','','','','','소','중','대','빨강','검정'];
/******************************************************************
 * HISTORY 
 *****************************************************************/
function MakeHistoryDetailMap() {
	if(typeof(history_detail_arr) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_1) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_1.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_1 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_1.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_2) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_2.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_2 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_2.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_3) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_3.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_3 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_3.push([{},{},{},{},{},{}]) 
		}
	}
}

function DrawHistoryBackground() {
	var tbl = document.createElement("table");
	tbl.id = "history_simple_record";
	tbl.summary = "history simple record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-simple").append(tbl);
	
	for (var i = 0; i < 6; i++) {
		var tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (var j = 0; j < 310; j++) {
			var td = document.createElement("td");
			td.id = "history-simple-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	tbl.className = "history_record";

	tbl = document.createElement("table");
	tbl.id = "history_detail_record";
	tbl.summary = "history detail record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-detail").append(tbl);

	for (i = 0; i < 6; i++) {
		tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (j = 0; j < 310; j++) {
			td = document.createElement("td");
			td.id = "history-detail-" + j + "-" + i;
			tr.appendChild(td);
		}
	}


	
	tbl = document.createElement("table");
	tbl.id = "history-simple-detail";
	tbl.summary = "history simple detail";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-simple-detail").append(tbl);
	
	for (var i = 0; i < 3; i++) {
		var tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (var j = 0; j < 310; j++) {
			var td = document.createElement("td");
			td.id = "history-simple-detail-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	tbl.className = "history-simple-detail";
}
function RenderHistoryDetail(result) {
	if (shoe_change_check === true){
		var x = 0;
		var y = 0;		
		history_pre_data = null;
		shoe_change_check = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}

	if (history_pre_data == null) {
		x = 0;
		y = 0;			
		history_last_x = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data.win || history_last_win == result.win) {
			var nx = history_pre_data.x;
			var ny = history_pre_data.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny - 1;
				temp_history_plus++
			} else {
				if (history_last_x != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus = 0;
				} else {
					var below_obj = history_detail_arr[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
					}
					temp_history_plus = 0;						
				}
			}
		} else {
			x = history_last_x + 1;
			history_last_x = x;
			y = 0;
			temp_history_plus = 0;
		}
	}

	//ClearHistory
	// if (x > 57) {
	// 	$("#history_detail td *").remove();
	// 	x = 0;
	// 	y = 0;
	// 	history_last_x = 0;
	// 	make_history_detail_map();
	// }
	
	history_pre_data = {
		'x' : x, 
		'y' : y, 
		'win' : result.win, 
		'pair' : result.pair
	};
	
	history_detail_arr[x][y] = history_pre_data;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	//var detail_img = 'detail_' + result.win.toLowerCase() + '.png';
	
	if (result.win != 'T') {
		history_last_win = result.win;
	}
	
	$("#history-detail-" + x + "-" + y).html('<div class="history-icon myb-'+result.turn+'" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');

	//중국점 추가를 위해 테스트 배열
	if(result.win == 'T') {
		return;
	} else {
		if (last_result != result.win) {
			j_point = 0;
			j_point = j_point + 1;
			c_point.push(j_point);
			c_point_v = c_point_v + 1
		}

		//내용이 같은것은 아래로~
		if (last_result == result.win) {
			j_point = j_point + 1;
			c_point.pop();
			c_point.push(j_point);
		}
		last_result = result.win;
	}
	//중국점을 그려줍세
	
	//1군을 그려줍시다
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw == 1) {	
		if (c_point_v[c_point_v+1] > 0) {
			
			c_point_v = c_point_v + 1;
		}	
		
		if(c_point[c_point_v] > 1) {
			c_point_cnt = c_point[c_point_v - 1] - c_point[c_point_v];
			
			if (c_point_cnt == -1) {
				RenderHistoryDetail1({
					'win':'P',
					'pair':'N'
				});
			} else {
				RenderHistoryDetail1({
					'win':'B',
					'pair':'N'
				});
			}
		} else {
			c_point_cnt = c_point[c_point_v - 1] - c_point[c_point_v - 2];
			
			if (c_point_cnt == 0) {
				RenderHistoryDetail1({
					'win':'B',
					'pair':'N'
				});
			} else {
				RenderHistoryDetail1({
					'win':'P',
					'pair':'N'
				});
			}
		}
	} else {
		//sw = 0
		if (c_point[1] > 0) {
			c_point_sw = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt = c_point[c_point_v-1] - i;
				
				if (c_point_cnt > 0 || c_point_cnt > -1) {
					RenderHistoryDetail1({
						'win':'B',
						'pair':'N'
					});
				} else {
					RenderHistoryDetail1({
						'win':'P',
						'pair':'N'
					});
				}
				c_point_v = c_point_v + 1;
			}				
		} else {
			return;
		}
	}
	
	//2군을 그려줍시다
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw_2 == 1) {			
		if(c_point[c_point_v] > 1) {
			c_point_cnt_2 = c_point[c_point_v - 2] - c_point[c_point_v];
			
			if (c_point_cnt_2 == -1) {
				RenderHistoryDetail2({
					'win':'P',
					'pair':'N'
				});
			} else {
				RenderHistoryDetail2({
					'win':'B',
					'pair':'N'
				});
			}
		} else {
			c_point_cnt_2 = c_point[c_point_v - 1] - c_point[c_point_v - 3];
			
			if (c_point_cnt_2 == 0) {
				RenderHistoryDetail2({
					'win':'B',
					'pair':'N'
				});
			} else {
				RenderHistoryDetail2({
					'win':'P',
					'pair':'N'
				});
			}
		}
	} else {
		//sw = 0
		if (c_point[2] > 0) {
			c_point_sw_2 = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt_2 = c_point[c_point_v-2] - i;
				
				if (c_point_cnt_2 > 0 || c_point_cnt_2 > -1) {
					RenderHistoryDetail2({
						'win':'B',
						'pair':'N'
					});
				} else {
					RenderHistoryDetail2({
						'win':'P',
						'pair':'N'
					});
				}
			//c_point_v = c_point_v + 1;
			}				
		} else {
			return;
		}
	}
	
	//3군을 그려줍시다
	//일단 카운팅을 하여 시작 지점 잡아주기.
	if(c_point_sw_3 == 1) {			
		if(c_point[c_point_v] > 1) {
			c_point_cnt_3 = c_point[c_point_v - 3] - c_point[c_point_v];
			
			if (c_point_cnt_3 == -1) {
				RenderHistoryDetail3({
					'win':'P',
					'pair':'N'
				});
			} else {
				RenderHistoryDetail3({
					'win':'B',
					'pair':'N'
				});
			}
		} else {
			c_point_cnt_3 = c_point[c_point_v - 1] - c_point[c_point_v - 4];
			
			if (c_point_cnt_3 == 0) {
				RenderHistoryDetail3({
					'win':'B',
					'pair':'N'
				});
			} else {
				RenderHistoryDetail3({
					'win':'P',
					'pair':'N'
				});
			}
		}
	} else {
		//sw = 0
		if (c_point[3] > 0) {
			c_point_sw_3 = 1;
			for (var i = 2; i <= c_point[c_point_v];i++){
				c_point_cnt_3 = c_point[c_point_v-2] - i;
				
				if (c_point_cnt_3 > 0 || c_point_cnt_3 > -1) {
					RenderHistoryDetail3({
						'win':'B',
						'pair':'N'
					});
				} else {
					RenderHistoryDetail3({
						'win':'P',
						'pair':'N'
					});
				}
			}				
		} else {
			return;
		}
	}
}

function RenderHistoryDetail1(result) {
	//return;
	if (shoe_change_check_1 === true){
		var x = 0;
		var y = 0;		
		history_pre_data_1 = null;
		
		shoe_change_check_1 = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data_1 == null) {
		x = 0;
		y = 0;			
		history_last_x_1 = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data_1.win) {
			var nx = history_pre_data_1.x;
			var ny = history_pre_data_1.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny-1;
				temp_history_plus_1++
			} else {
				if (history_last_x_1 != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus_1 = 0;
				} else {
					var below_obj = history_detail_arr_1[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
						temp_history_plus_1 = 0;
					}						
				}
			}
		} else {
			x = history_last_x_1 + 1;
			history_last_x_1 = x;
			y = 0;
			temp_history_plus_1 = 0;
		}
	}
	
	history_pre_data_1 = {
		'x':x, 
		'y':y, 
		'win':result.win, 
		'pair' : result.pair
	};			
	
	history_detail_arr_1[x][y] = history_pre_data_1;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'ch_1_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-1-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
}

function RenderHistoryDetail2(result) {
	//return;
	if (shoe_change_check_2 === true){
		var x = 0;
		var y = 0;		
		history_pre_data_2 = null;
		
		shoe_change_check_2 = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data_2 == null) {
		x = 0;
		y = 0;			
		history_last_x_2 = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data_2.win || (history_last_win_2 == result.win)) {
			var nx = history_pre_data_2.x;
			var ny = history_pre_data_2.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny-1;
				temp_history_plus_2++
			} else {
				if (history_last_x_2 != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus_2 = 0;
				} else {
					var below_obj = history_detail_arr_2[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
						temp_history_plus_2 = 0;
					}						
				}
			}
			
		} else {
			x = history_last_x_2 + 1;
			history_last_x_2 = x;
			y = 0;
			temp_history_plus_2 = 0;
		}
	}
	
	history_pre_data_2 = {
		'x':x, 
		'y':y, 
		'win':result.win, 
		'pair' : result.pair
	};			
	
	history_detail_arr_2[x][y] = history_pre_data_2;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'ch_2_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	
	// if (result.win != 'T')
	// 	history_last_win = result.win;
	
	$("#history-detail-2-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
}

function RenderHistoryDetail3(result) {
	//return;
	if (shoe_change_check_3 === true){
		var x = 0;
		var y = 0;		
		history_pre_data_3 = null;
		
		shoe_change_check_3 = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}
	if (history_pre_data_3 == null) {
		x = 0;
		y = 0;			
		history_last_x_3 = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data_3.win || (history_last_win_3 == result.win)) {
			var nx = history_pre_data_3.x;
			var ny = history_pre_data_3.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny-1;
				temp_history_plus_3++
			} else {
				if (history_last_x_3 != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus_3 = 0;
				} else {
					var below_obj = history_detail_arr_3[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
						temp_history_plus_3 = 0;
					}						
				}
			}
			
		} else {
			x = history_last_x_3 + 1;
			history_last_x_3 = x;
			y = 0;
			temp_history_plus_3 = 0;
		}
	}
	
	history_pre_data_3 = {
		'x':x, 
		'y':y, 
		'win':result.win, 
		'pair' : result.pair
	};			
	
	history_detail_arr_3[x][y] = history_pre_data_3;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'ch_3_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	$("#history-detail-3-" + x + "-" + y).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/'+detail_img+'\')"></div>');
}

function ClearHistory() {
	$("#history-detail td *").remove();
	$("#history-detail-1 td *").remove();
	$("#history-detail-2 td *").remove();
	$("#history-detail-3 td *").remove();
	history_record = [];
	history_record_detail = [];
	history_simple_idx = -1;
	history_pre_data = null;
	history_last_win = '';
	history_last_x = -1;
	history_detail_arr = [];
	temp_history_plus = 0;
	temp_history_plus_1 = 0;
	temp_history_plus_2 = 0;
	temp_history_plus_3 = 0;
	history_record_detail_1 = [];
	history_pre_data_1 = null;
	history_last_win_1 = '';
	history_last_x_1 = -1;
	history_detail_arr_1 = [];
	real_count= 1;
	history_record_detail_2 = [];
	history_pre_data_2 = null;
	history_last_win_2 = '';
	history_last_x_2 = -1;
	history_detail_arr_2 = [];
	real_count= 1;
	history_record_detail_3 = [];
	history_pre_data_3 = null;
	history_last_win_3 = '';
	history_last_x_3 = -1;
	history_detail_arr_3 = [];
	real_count= 1;
	c_point=[];
	j_point=[];
	c_point_sw = 0;
	c_point_count = 0;
	c_point_result = 0;
	c_point_cnt = 0;
	c_point_v = -1;
	last_result = null;
	simple_x = 0;
	simple_y = 0;
	history_counts_pixel = 0;
	history_counts = 1;
	shoe_change_check = 0;
	shoe_change_check_1 = 0;
	shoe_change_check_2 = 0;
	shoe_change_check_3 = 0;
	c_point_sw_1 = null;
	c_point_sw_2 = null;
	c_point_sw_3 = null;
	MakeHistoryDetailMap();
}

/******************************************************************
 * CHAT
 *****************************************************************/
function ChatInit() {
	// LoadScript(CONFIG.ASSETS+'/js/socket.io.js', function () {
	// 	socket = io.connect('https://socket.8008.company', {
	// 		"transports":['websocket'], 
	// 		"upgrade" : false,
	// 		"force new connection" : true,
	// 		"reconnection" : false
	// 	});
	// });
}
function ChatMessage(message, nickname) {
	$('.chat-list-box ul').append('<li><span style="color:#14a107">['+nickname+']</span> : '+message+'</li>');
	//ChatScroll();
}

function ChatScroll() {
	var objDiv = document.getElementById("chat-list-box");
	objDiv.scrollTop = objDiv.scrollHeight;
}

function NotificationMessage(message) {
	$('.message-box').text(message);
	$('.message-box').fadeIn()
	if(notification_message_interval != null) {
		clearInterval(notification_message_interval);
	}

	notification_message_interval = setInterval(function () {
		$('.message-box').fadeOut();
	},3000)
	setTimeout(function () {
		clearInterval(notification_message_interval);
	},4000)
	
}
function LogMessage(message, nickname) {
	$('.log-list-box ul').append('<li><span style="color:#14a107">['+nickname+']</span> : '+message+'</li>');
}

/******************************************************************
 * SOUND
 *****************************************************************/
function SoundPlay (sound) {
	if(sound_checker === false) {
		return;
	}
	if ($("#SOUND-"+sound).length == 0) {
		$("body").append(
			$("<audio>").attr("id","SOUND-"+sound)
				.append($("<source>")
				.attr("src",CONFIG.ASSETS+"/sound/"+sound+".mp3?v=200")
				.attr("type","audio/mpeg")
			)
		);
	}
	$("#SOUND-"+sound).get(0).play();
}

/******************************************************************
 * COUNTDOWN
 *****************************************************************/
function ClockLoad() {
	LoadScript(CONFIG.ASSETS+'/js/flipclock.min.js', function () {
		clock = new FlipClock($('#count-down'), 60, {clockFace: 'Counter'});
	},false)
}

function CountDown(time) {
	
	if(time > 0) {
		$('#count-down').css('opacity', 1);
		$('.chip-box-outer').fadeIn()
		$('.chip-box-inner').fadeIn();
		$('.chip').fadeIn();
		$('.notice').fadeOut();
		clock.setCounter(time);
	} else {
		$('#count-down').css('opacity', 0);
		$('.btn-confirm-box').fadeOut();
		$('.chip').fadeOut();
		$('.notice').fadeIn()
		clock.setCounter(60);
	}
}
/************************************************************
 * CardBox
 ***********************************************************/
function CardBox(type) {
	if(type == 'show') {
		$('.card-box-blind').fadeIn();
		$('.card-box').fadeIn();
		$('.cards').fadeIn();
	} else {
		$('.card-box-blind').fadeOut();
		$('.card-box').fadeOut();
		$('.cards').fadeOut();
	}
}
/************************************************************
 * GetMyMoney
 ***********************************************************/
function GetMyMoney() {
	// PostJson('saisai_json.php?v=get_my_money', {}, function (returnData) {
	// 	if(returnData.ErrorCode == 0) {
	// 		$('#my-money').text(AddComma(returnData.Data.Money));
	// 		CONFIG.MEMBER.MONEY = returnData.Data.Money;
	// 	} else {
	// 		return 0;
	// 	}
	// })
}

/******************************************************************
 * GET GAME HISTORY
 *****************************************************************/
function GetHistory() {
	PostJson('json.php?v=history',{token:'abcd'}, function (returnData) {
		if(returnData.ErrorCode != 0) {
			//alert('데이터 오류로 인해 새로고침이 됩니다.');
			location.reload();
		} else {
			
			var records = returnData.Data;

			for(var i in records) {
				
				if(records[i]['status'] != '-') {
					var win = records[i]['center_odd'] == 'O' ? 'P' : 'B';

					var json_data = {
						win : win, 
						pair : 'N',
						pair_player : 'N',
						pair_banker : 'N',
						turn : records[i]['id'],
					}

					RenderHistoryDetail(json_data);
				}
			}
			LoadScript(CONFIG.ASSETS+'/js/iscroll.js', function () {
				scroll_detail = new IScroll('#history-detail', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

				scroll_detail_1 = new IScroll('#history-detail-1', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

				scroll_detail_2 = new IScroll('#history-detail-2', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

				scroll_detail_3 = new IScroll('#history-detail-3', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	
			});
			
			setTimeout(function () {
				if(history_pre_data.x > 5) {
					scroll_detail.scrollTo( ( (history_pre_data.x-5) * 20) * -1, 0);	
				}

				if(history_pre_data_1.x > 5) {
					scroll_detail_1.scrollTo( ( (history_pre_data_1.x-5) * 7) * -1, 0);	
				}

				if(history_pre_data_2.x > 5) {
					scroll_detail_2.scrollTo( ( (history_pre_data_2.x-5) * 7) * -1, 0);	
				}

				if(history_pre_data_3.x > 5) {
					scroll_detail_3.scrollTo( ( (history_pre_data_3.x-5) * 7) * -1, 0);	
				}
				
			},1000)
		}
	});
}

function ToggleWinner(oe, number, pattern, color, left_number, right_number) {
	$('.win-'+oe.toLowerCase()).addClass("flicker").addClass("win-button");
	$('.win-c-'+color.toLowerCase()).addClass("flicker").addClass("win-button");
	$('.win-'+pattern.toLowerCase()).addClass("flicker").addClass("win-button");
	
	if(parseInt(number) >= 1 && parseInt(number) <= 4) {
		$('.win-low').addClass("flicker").addClass("win-button");
	} else if(parseInt(number) >= 5 && parseInt(number) <= 8) {
		$('.win-middle').addClass("flicker").addClass("win-button");
	} else if(parseInt(number) >= 9 && parseInt(number) <= 12) {
		$('.win-high').addClass("flicker").addClass("win-button");
	}
	var numbers = [left_number, right_number];
	var calculate = numbers.sort(function(a, b) {
		return a - b;
	});
	console.log(calculate[0], calculate[1], number)
	//고배당 12.5
	if(calculate[0] == calculate[1]) {
		if(calculate[0] == parseInt(number))
		{
			$('.btn-special').addClass("flicker").addClass("win-button");
		}
	} else {
		if(calculate[0] < parseInt(number) && calculate[1] > parseInt(number) ) {
			console.log('OK!')
			$('.btn-special').addClass("flicker").addClass("win-button");
		}	
	}
	setTimeout(function(){
			$('.win-button').each(function (index, el) {
				$(el).removeClass("flicker");
			});
			$('.win-button').removeClass('win-button');
	},5000);
}

/****************************************************************************************************
 * EVENT
 ***************************************************************************************************/
$('.show-bet-panel').on('click', function () {
	$('.card-box-blind').fadeOut();
	$('.card-box').fadeOut();
	$('.cards').fadeOut();
})

$('.show-card-box').on('click', function () {
	$('.card-box-blind').fadeIn();
	$('.card-box').fadeIn();
	$('.cards').fadeIn();
});

$('.input-box-text').on('keypress', function (event) {
	var message = ($('.input-box-text').val())
	if(event.charCode == 13 || event.keyCode == 13 || event.which == 13) {
		if(message == '' || message.length <=0 ) {
			return;
		}

		//ChatMessage(message, '채팅');
		ChatMessage('데모버전에서는 채팅을이용할수없습니다.', '채팅');
		$('.input-box-text').val('')
		ChatScroll();
	}
});

$('#show-history').on('click', function () {
	$('.log-box').hide();
	$('.chat-box').hide();
	$('.history-box').show();
})

$('#show-chat').on('click', function () {
	$('.log-box').hide();
	$('.chat-box').show();
	$('.history-box').hide();
})

$('#show-log').on('click', function () {
	$('.log-box').show();
	$('.chat-box').hide();
	$('.history-box').hide();
})

$('.send-button').on('click', function () {
	var message = ($('.input-box-text').val())
	if(message == '' || message.length <=0 ) {return;}
	ChatMessage('데모버전에서는 채팅을이용할수없습니다.', '채팅');
	$('.input-box-text').val('');
})

$('.btn-bet').hover(function() {
	$(this).css("background-image",$(this).css("background-image").replace(/\_off\.png/, '\_on\.png'));
}, function() { 
	$(this).css("background-image",$(this).css("background-image").replace(/\_on\.png/, '\_off\.png'));
});

$('.second-btn-box').hover(function() {
	$(this).attr('src',$(this).attr("src").replace(/\_off\.png/, '\_on\.png'));
}, function() { 
	$(this).attr('src',$(this).attr("src").replace(/\_on\.png/, '\_off\.png'));
});

$('.chip-btn').on('click', function () {
	SoundPlay('chip');
	$('.chip-btn-money').each(function (index,el) {
		$(el).removeClass('flicker');
	});

	$('.chip-btn').each(function (index,el) {
		$(el).attr('src', $(el).attr('src').replace(/\_on\.png/, '\_off\.png'));
		$(el).removeClass('flicker');
	});
	
	var money = $(this).data('money');
	var money_location = $(this).data('location');
	if($(this).attr('src').indexOf('_on.png') != -1) {
		$(this).attr('src', $(this).attr('src').replace(/\_on\.png/, '\_off\.png'));
		selected_chip = 0;
		$(this).removeClass('flicker');
	} else {
		$(this).attr('src', $(this).attr('src').replace(/\_off\.png/, '\_on\.png'))
		if(money != selected_chip) {
			ChatMessage(AddComma(money)+'칩을 선택하였습니다.','시스템');
			selected_chip = money;
			selected_chip_location = money_location;
		}
		$(this).addClass('flicker');
	}
})

$('.btn-chip-ok').on('click', function () {
	$('.btn-confirm-box-two').fadeOut();
	$('.btn-confirm-box-one').fadeIn();
	PostJson('saisai_json.php?v=betting', {bettings : betting_money_arr}, function (returnData) {
		if(returnData.ErrorCode != 0)
		{
			// betting_money_arr = [0,0,0,0,0,0];
			// $('.bet-money').text(0);
			LogMessage('이미 베팅한 내역이있거나 실패 하였습니다.', '시스템');
			
		}
		else
		{
			_.each(betting_money_arr, function (row,index) {
				if(row > 0) {
					LogMessage(betting_location_label[index]+'위치에 '+AddComma(row)+'원을 베팅 성공하였습니다.', '시스템');	
				}
			});
			betting_confirm = true;
			_.each(betting_money_arr_confirm, function (row,index) {
				betting_money_arr_confirm[index] =betting_money_arr_confirm[index] + betting_money_arr[index];
				
			});

			betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			_.each(betting_money_arr_confirm, function (row,index) {
				if(row > 0) {
					$('.bet-money-'+index).text( AddComma(betting_money_arr_confirm[index]));
				}
			});
		}
	});
});

$('.btn-bet').on('click', function () {
	$(this).addClass("btn-bet-temp");
	SoundPlay('chip_on');
	var label = $(this).data('label');
	var location = $(this).data('location');
	if(selected_chip <= 0) {
		ChatMessage('<span style="color:#ff5251">칩을 선택해주세요</span>', '시스템');
	} else {
		if(CONFIG.MEMBER.MONEY < selected_chip) {
			ChatMessage('<span style="color:#ff5251">보유머니가 부족합니다.</span>', '시스템');
		} else {
			if(special_rate === false)
			{
				ChatMessage('<span style="color:#ff5251">배당이 없음으로 베팅이 불가능합니다.</span>', '시스템');
				return;
			}

			if( (SumArray(betting_money_arr) + selected_chip) > CONFIG.MEMBER.MONEY) {
				ChatMessage('<span style="color:#ff5251">보유머니가 부족합니다.</span>', '시스템');
			}
			else
			{
				betting_money_arr[location] = betting_money_arr[location] + selected_chip;
				$('.bet-money-'+location).text( AddComma(betting_money_arr[location]));
			}
		}
	}
});

$('.input-box-text').on('keypress', function (event) {
	var message = ($('.input-box-text').val())
	if(event.charCode == 13 || event.keyCode == 13 || event.which == 13) {
		if(message == '' || message.length <=0 ) {
			return;
		}

		//ChatMessage(message, '채팅');
		ChatMessage('데모버전에서는 채팅을이용할수없습니다.', '채팅');
		$('.input-box-text').val('')
		ChatScroll();
	}
})

$('.btn-chip-cancel').on('click', function () {
	$('.btn-confirm-box-one').show();
	$('.btn-confirm-box-two').hide();
});
$('.send-button').on('click', function () {
	var message = ($('.input-box-text').val())
	if(message == '' || message.length <=0 ) {return;}
	ChatMessage('데모버전에서는 채팅을이용할수없습니다.', '채팅');
	$('.input-box-text').val('');
})

$('.btn-chip-reset').on('click', function () {
	betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	$(".bet-money").text(0);
	//ChatMessage('금액 초기화를 하였습니다.', '시스템');
	_.each(betting_money_arr_confirm, function (row,index) {
		$('.bet-money-'+index).text(AddComma(betting_money_arr_confirm[index]));
	});
	$(".btn-bet").removeAttr("style");
	$(".btn-bet-temp").removeClass("btn-bet-temp");
})

$('.btn-chip-cancel').on('click', function () {
	$('.chip-box').fadeIn();
	$('.bet-confirm-box').fadeOut();
	btn_chip_confirm_checker = false;
});

$('.btn-chip-confirm').on('click', function () {
	$('.chip-box').fadeOut();
	$('.bet-confirm-box').fadeIn();
	btn_chip_confirm_checker = true;
});

$('.btn-chip-confirm').on('click', function () {
	$(".btn-bet").removeAttr("style");
	PostJson('saisai_json.php?v=betting', {bettings : betting_money_arr}, function (returnData) {
		if(returnData.ErrorCode != 0)
		{
			// betting_money_arr = [0,0,0,0,0,0];
			// $('.bet-money').text(0);
			ChatMessage('이미 베팅한 내역이있거나 실패 하였습니다.', '시스템');
		}
		else
		{
			_.each(betting_money_arr, function (row,index) {
				if(index == 0) {
					if(row > 0) {
						ChatMessage(betting_location_label[special_loc]+'위치에 '+AddComma(row)+'원을 베팅 성공하였습니다.', '시스템');	
					}
				} else {
					if(row > 0) {
						ChatMessage(betting_location_label[index]+'위치에 '+AddComma(row)+'원을 베팅 성공하였습니다.', '시스템');	
					}
					
				}
			});
			betting_confirm = true;
			_.each(betting_money_arr_confirm, function (row,index) {
				betting_money_arr_confirm[index] =betting_money_arr_confirm[index] + betting_money_arr[index];
				
			});
			$(".btn-bet").css("background-color","rgb(73, 73, 73)");
			$(".btn-bet-temp").removeAttr("style");
			betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			_.each(betting_money_arr_confirm, function (row,index) {
				if(row > 0) {
					$('.bet-money-'+index).text( AddComma(betting_money_arr_confirm[index]));
				}
			});
			$('.bet-confirm-box').fadeOut();
			btn_chip_confirm_checker = false;
		}
	});
});

var PlayLoop = setInterval(function () {
	PostJson('saisai_json.php?v=play',{}, function (returnData) {
		if(returnData.ErrorCode != 0) {
			location.reload();
		} else {
			GetMyMoney();
			var data = returnData.Data;
			if(data == null) {
				return;
			}
			
			status = data['status'];

			if(data['shuffle'] == 'Y')
			{
				NotificationMessage('카드 셔플중입니다.. 약 5분동안 진행됩니다.');
			}

			if(turn == 0 ) {
				turn = data['turn'];
			}

			if(turn != data['turn']) {
				turn = data['turn'];
				$('.left-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/cardback2.png")').removeClass('left-card-show');
				$('.center-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/cardback2.png")').removeClass('center-card-show');
				$('.right-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/cardback2.png")').removeClass('right-card-show');
				$('.bet-money').text(0);
			}
			
			if(data['rate'] === false) {
				special_rate = false;
				special_loc = false;
				$('.special-rate').text('배당없음');
			} else {
				special_rate = special_rates[data['rate']];
				special_loc = data['rate'];
				$('.special-rate').text(special_rates[data['rate']]);
			}

			$('.turn-label').text(AddComma(data['turn'])+'회차 베팅로그');

			//COUNT
			if(data['count'] > 0) {

				if(first_mesage_checker === false) {
					first_mesage_checker = true;
					//ChatMessage(data['turn'] +'회차 베팅시작 되었습니다.', '시스템');
				}

				if(data['count'] <= 10 && data['count'] >= 1) {
					SoundPlay(data['count']);
				}

				if(first_notification_checker === false) {
					first_notification_checker = true;
					NotificationMessage(AddComma(data['turn']) +'회차 베팅시작 되었습니다.', '시스템')
					ChatMessage(AddComma(data['turn']) +'회차 베팅시작 되었습니다.', '시스템')
					SoundPlay('betting_start');
					CardBox('hide');
					$('.btn-confirm-box').fadeIn();
				}
				CountDown(data['count'])	
			} else {
				if(first_mesage_checker === true) {
					first_mesage_checker = false;
					NotificationMessage(AddComma(data['turn']) +'회차 베팅종료 되었습니다.');
					ChatMessage(AddComma(data['turn']) +'회차 베팅종료 되었습니다.', '시스템');
					SoundPlay('betting_end');
					CardBox('show');
				$('.betting-overlay').show();
					
					$('.card-box').fadeIn();
					$('.cards').fadeIn();
				}
				CountDown(0);
			}

			if(data['center_card'] !== false) {
				$('.center-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['center_card'].toLowerCase()+'.png")');
				$('.center-card').addClass('center-card-show');
			}

			if(data['left_card'] !== false) {
				$('.left-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['left_card'].toLowerCase()+'.png")');
				$('.left-card').addClass('left-card-show');
				if(parseInt(data['left_number']) == 1) {
					$('.left-number').text('A');	
				} else if(parseInt(data['left_number']) == 11) {
					$('.left-number').text('J');
				} else if(parseInt(data['left_number']) == 12) {
					$('.left-number').text('Q');
				} else {
					$('.left-number').text(data['left_number']);
				}
			}

			if(data['right_card'] !==  false) {
				$('.right-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['right_card'].toLowerCase()+'.png")');
				$('.right-card').addClass('right-card-show');
				if(parseInt(data['right_number']) == 1) {
					$('.right-number').text('A');	
				} else if(parseInt(data['right_number']) == 11) {
					$('.right-number').text('J');
				} else if(parseInt(data['right_number']) == 12) {
					$('.right-number').text('Q');
				} else {
					$('.right-number').text(data['right_number']);
				}
			}

			if(data['status'] == 'COMPLETED' && data['result'] != '-') {
				$('.betting-overlay').hide();
				$(".btn-bet").removeAttr("style");
				$(".btn-bet-temp").removeClass("btn-bet-temp");
				//$('.win-box img').attr('src', CONFIG.ASSETS+'/img/win/winner_'+data['result'].toLowerCase()+'.png?v=600');
				//$('.win-box').fadeIn();
				// if(data['shoebox_change'] == 'N')
				// {
				// 	$('.win-'+data['result'].toLowerCase()).css("background-image",$('.win-'+data['result'].toLowerCase()).css("background-image").replace(/\_off\.png/, '\_on\.png'));
				// }
				// else
				// {
				// 	$('.win-box').hide();
				// 	$('.card-box-blind').fadeOut()
				// 	$('.card-box').fadeOut()
				// }
			
				if($('.myb-'+data['turn']).length <= 0) {
					var win = data['center_odd'] == 'O' ? 'P' : 'B';

					var json_data = {
						win : win, 
						pair : 'N',
						turn : data['turn']
					}

					RenderHistoryDetail(json_data);

					//ChatMessage(AddComma(data['turn'])+'회차 '+win_label[data['result']]+'가 이겼습니다.', '시스템');
					//SoundPlay('winner_'+data['result'].toLowerCase());
					setTimeout(function () {
						if(history_pre_data.x > 5) {
							scroll_detail.scrollTo( ( (history_pre_data.x-5) * 20) * -1, 0);	
						}

						if(history_pre_data_1.x > 5) {
							scroll_detail_1.scrollTo( ( (history_pre_data_1.x-5) * 7) * -1, 0);	
						}

						if(history_pre_data_2.x > 5) {
							scroll_detail_2.scrollTo( ( (history_pre_data_2.x-5) * 7) * -1, 0);	
						}

						if(history_pre_data_3.x > 5) {
							scroll_detail_3.scrollTo( ( (history_pre_data_3.x-5) * 7) * -1, 0);	
						}
					},1000);
					ToggleWinner(data['center_odd'], data['center_number'], data['center_pattern'], data['center_color'], data['left_number'], data['right_number']);
					
					first_notification_checker = false;
					betting_money_arr_confirm = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
				}
			}
		}
	})
},1000)

ClockLoad();
MakeHistoryDetailMap();
DrawHistoryBackground();
GetHistory();

var canvas;
canvas = document.getElementById('videoCanvas');
ctx = canvas.getContext("2d");
var client = new WebSocket('wss://video2.8008.company/');
player = new jsmpeg(client, {canvas:canvas});
