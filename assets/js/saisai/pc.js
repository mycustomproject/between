$(document).ready(function () {
var clock = null;
var history_record = [];
var history_record_detail = [];
var history_simple_idx = -1;
var history_simple_detail_idx = -1;
var history_pre_data = null;
var history_last_win = '';
var history_last_x = -1;
var history_detail_arr = [];
var temp_history_plus = 0;
var temp_history_plus_1 = 0;
var temp_history_plus_2 = 0;
var temp_history_plus_3 = 0;

var history_record_detail_1 = [];
var history_pre_data_1 = null;
var history_last_win_1 = '';
var history_last_x_1 = -1;
var history_detail_arr_1 = [];
var real_count= 1;

var history_record_detail_2 = [];
var history_pre_data_2 = null;
var history_last_win_2 = '';
var history_last_x_2 = -1;
var history_detail_arr_2 = [];
var real_count= 1;

var history_record_detail_3 = [];
var history_pre_data_3 = null;
var history_last_win_3 = '';
var history_last_x_3 = -1;
var history_detail_arr_3 = [];
var real_count= 1;

var c_point=[];
var j_point=[];
var c_point_sw = 0;
var c_point_count = 0;
var c_point_result = 0;
var c_point_cnt = 0;
var c_point_v = -1;
var last_result = null;
var simple_x = 0;
var simple_y = 0;
var history_counts_pixel = 0;
var history_counts = 1;
var shoe_change_check = 0;
var shoe_change_check_1 = 0;
var shoe_change_check_2 = 0;
var shoe_change_check_3 = 0;
var c_point_sw_1 = null;
var c_point_sw_2 = null;
var c_point_sw_3 = null;
var shoebox = 0;
var scroll_simple = null;
var scroll_detail = null;
var scroll_simple_detail = null;
var selected_chip = 0;

var first_notification_checker = false;
var first_mesage_checker = false;
var btn_chip_confirm_checker = false;

var sound_checker = true;

var notification_message_interval = null;
var turn = 0;

var special_rate = false;
var special_loc = false;
var special_rates = [12.5,11.5,5.8,3.7,2.87,2.15,1.93,1.62,1.42,1.25,1.15,1.93,1.93,3.7,3.7,3.7,3.7,3.7,3.7,3.7,3.7,2.7,2.7,2.7,1.93,1.93];


var betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var betting_money_arr_confirm = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var betting_location_label = ['사이잡기x12.5','사이잡기x11.5','사이잡기x5.8','사이잡기x3.7','사이잡기x2.87','사이잡기x2.15','사이잡기x1.93','사이잡기x1.62','사이잡기x1.42','사이잡기x1.25','사이잡기x1.15','홀','짝','스페이드','다이아몬드','하트','크로바','','','','','소','중','대','빨강','검정'];
/******************************************************************
 * HISTORY 
 *****************************************************************/
function MakeHistoryDetailMap() {
	if(typeof(history_detail_arr) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_1) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_1.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_1 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_1.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_2) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_2.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_2 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_2.push([{},{},{},{},{},{}]) 
		}
	}

	if(typeof(history_record_detail_3) != 'undefined') {
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_3.push([{},{},{},{},{},{}]) 
		}
	} else {
		history_detail_arr_3 = [];
		for(var i = 0; i < 541;i++) { 
			var map = [{},{},{},{},{},{}];
			history_detail_arr_3.push([{},{},{},{},{},{}]) 
		}
	}
}

function DrawHistoryBackground() {
	var tbl = document.createElement("table");
	
	tbl.id = "history_simple_record";
	tbl.summary = "history simple record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-simple").append(tbl);
	
	for (var i = 0; i < 6; i++) {
		var tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (var j = 0; j < 310; j++) {
			var td = document.createElement("td");
			td.id = "history-simple-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	tbl.className = "history_record";

	tbl = document.createElement("table");
	tbl.id = "history_detail_record";
	tbl.summary = "history detail record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-detail").append(tbl);

	for (i = 0; i < 6; i++) {
		tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (j = 0; j < 310; j++) {
			td = document.createElement("td");
			td.id = "history-detail-" + j + "-" + i;
			tr.appendChild(td);
		}
	}

	tbl.className = "history-detail-record";

	tbl = document.createElement("table");
	
	tbl.id = "history_simple_detail_record";
	tbl.summary = "history simple detail record";
	tbl.cellPadding = "0";
	tbl.cellSpacing = "0";

	$("#history-simple-detail").append(tbl);
	
	for (var i = 0; i < 3; i++) {
		var tr = document.createElement("tr");
		tbl.appendChild(tr);
		for (var j = 0; j < 310; j++) {
			var td = document.createElement("td");
			td.id = "history-simple-detail-" + j + "-" + i;
			tr.appendChild(td);
		}
	}
	tbl.className = "history_simple_detail_record";
}

function RenderHistorySimpleDetail(result) {
	if (result.pair == 'N') {
		result.pair = '';
	}
	
	if (history_simple_detail_idx >= 510) {
		$("#history-simple-detail td *").remove();
		history_simple_idx = -1;
	}
	
	history_simple_detail_idx++;
	if (history_simple_detail_idx > 510) {
		history_simple_detail_idx = (history_simple_detail_idx % 90); 
	}
	

	var turn_pos = history_simple_detail_idx;				

	var y = Math.floor(turn_pos / 3);		 
	
	if (turn_pos < 3) { 
		var x = turn_pos;
	} else {
		var x = turn_pos % 3;
	}

	var simple_img = 'simple_'+result.win.toLowerCase() + '.png?v=600';		
	
	$("#history-simple-detail-" + y + "-" + x).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/saisai/'+simple_img+'\')"></div>');
	
	simple_x = x;
	simple_y = y
}
function RenderHistorySimple(result) {
	if (result.pair == 'N') {
		result.pair = '';
	}
	
	if (history_simple_idx >= 510) {
		$("#history-simple td *").remove();
		history_simple_idx = -1;
	}
	
	history_simple_idx++;
	if (history_simple_idx > 510) {
		history_simple_idx = (history_simple_idx % 90); 
	}
	

	var turn_pos = history_simple_idx;				

	var y = Math.floor(turn_pos / 6);		 
	
	if (turn_pos < 6) { 
		var x = turn_pos;
	} else {
		var x = turn_pos % 6;
	}

	var simple_img = 'simple_'+result.win.toLowerCase() + '.png?v=600';		
	
	$("#history-simple-" + y + "-" + x).html('<div class="history-icon" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/saisai/'+simple_img+'\')"></div>');
	
	simple_x = x;
	simple_y = y
}

function RenderHistoryDetail(result) {
	if (shoe_change_check === true){
		var x = 0;
		var y = 0;		
		history_pre_data = null;
		shoe_change_check = false;
	} else {
		var x = 0;
		var y = 0;		
	}
		
	if (result.pair == 'N') {
		result.pair = '';
	}

	if (history_pre_data == null) {
		x = 0;
		y = 0;			
		history_last_x = x;
	} else {
		if (result.win == 'T' || result.win == history_pre_data.win || history_last_win == result.win) {
			var nx = history_pre_data.x;
			var ny = history_pre_data.y + 1;
			if (ny > 5) {
				x = nx + 1;
				y = ny - 1;
				temp_history_plus++
			} else {
				if (history_last_x != nx) {
					x = nx + 1;
					y = ny-1;
					temp_history_plus = 0;
				} else {
					var below_obj = history_detail_arr[nx][ny];
					if (below_obj.x != undefined) {
						x = nx + 1;
						y = ny - 1;
					} else {
						x = nx;
						y = ny;
					}
					temp_history_plus = 0;						
				}
			}
		} else {
			x = history_last_x + 1;
			history_last_x = x;
			y = 0;
			temp_history_plus = 0;
		}
	}

	//ClearHistory
	// if (x > 57) {
	// 	$("#history_detail td *").remove();
	// 	x = 0;
	// 	y = 0;
	// 	history_last_x = 0;
	// 	make_history_detail_map();
	// }
	
	history_pre_data = {
		'x' : x, 
		'y' : y, 
		'win' : result.win, 
		'pair' : result.pair
	};
	
	history_detail_arr[x][y] = history_pre_data;
	
	var pair_str = '';
	if(result.pair == 'Y')
	{
		if(result.pair_player == 'Y')
		{
			pair_str += '_p';
		}

		if(result.pair_banker == 'Y')
		{
			pair_str += '_b';
		}
	}
	
	var detail_img = 'detail_' + result.win.toLowerCase() + pair_str + '.png?v=600';
	//var detail_img = 'detail_' + result.win.toLowerCase() + '.png';
	
	if (result.win != 'T') {
		history_last_win = result.win;
	}
	
	$("#history-detail-" + x + "-" + y).html('<div class="history-icon myb-'+result.turn+'" style="background-image:url(\''+CONFIG.ASSETS+'/img/history/saisai/'+detail_img+'\')">'+result.number+'</div>');	
}

function ClearHistory() {
	$("#history-detail td *").remove();
	$("#history-detail-1 td *").remove();
	$("#history-detail-2 td *").remove();
	$("#history-detail-3 td *").remove();
	history_record = [];
	history_record_detail = [];
	history_simple_idx = -1;
	history_pre_data = null;
	history_last_win = '';
	history_last_x = -1;
	history_detail_arr = [];
	temp_history_plus = 0;
	temp_history_plus_1 = 0;
	temp_history_plus_2 = 0;
	temp_history_plus_3 = 0;
	history_record_detail_1 = [];
	history_pre_data_1 = null;
	history_last_win_1 = '';
	history_last_x_1 = -1;
	history_detail_arr_1 = [];
	real_count= 1;
	history_record_detail_2 = [];
	history_pre_data_2 = null;
	history_last_win_2 = '';
	history_last_x_2 = -1;
	history_detail_arr_2 = [];
	real_count= 1;
	history_record_detail_3 = [];
	history_pre_data_3 = null;
	history_last_win_3 = '';
	history_last_x_3 = -1;
	history_detail_arr_3 = [];
	real_count= 1;
	c_point=[];
	j_point=[];
	c_point_sw = 0;
	c_point_count = 0;
	c_point_result = 0;
	c_point_cnt = 0;
	c_point_v = -1;
	last_result = null;
	simple_x = 0;
	simple_y = 0;
	history_counts_pixel = 0;
	history_counts = 1;
	shoe_change_check = 0;
	shoe_change_check_1 = 0;
	shoe_change_check_2 = 0;
	shoe_change_check_3 = 0;
	c_point_sw_1 = null;
	c_point_sw_2 = null;
	c_point_sw_3 = null;
	MakeHistoryDetailMap();
}

/******************************************************************
 * CHAT
 *****************************************************************/
function ChatInit() {
	// LoadScript(CONFIG.ASSETS+'/js/socket.io.js', function () {
	// 	socket = io.connect('https://socket.8008.company', {
	// 		"transports":['websocket'], 
	// 		"upgrade" : false,
	// 		"force new connection" : true,
	// 		"reconnection" : false
	// 	});
	// });
}
function ChatMessage(message, nickname) {
	$('.chat-list-box ul').append('<li><span style="color:#14a107">['+nickname+']</span> : '+message+'</li>');
	ChatScroll();
}

function ChatScroll() {
	var objDiv = document.getElementById("chat-list-box");
	objDiv.scrollTop = objDiv.scrollHeight;
}

function NotificationMessage(message) {
	$('.message-box').text(message);
	$('.message-box').fadeIn()
	if(notification_message_interval != null) {
		clearInterval(notification_message_interval);
	}

	notification_message_interval = setInterval(function () {
		$('.message-box').fadeOut();
	},3000)
	setTimeout(function () {
		clearInterval(notification_message_interval);
	},4000)
	
}

/******************************************************************
 * SOUND
 *****************************************************************/
function SoundPlay (sound) {
	if(sound_checker === false) {
		return;
	}
	if ($("#SOUND-"+sound).length == 0) {
		$("body").append(
			$("<audio>").attr("id","SOUND-"+sound)
				.append($("<source>")
				.attr("src",CONFIG.ASSETS+"/sound/"+sound+".mp3?v=200")
				.attr("type","audio/mpeg")
			)
		);
	}
	$("#SOUND-"+sound).get(0).play();
}

/******************************************************************
 * COUNTDOWN
 *****************************************************************/
function ClockLoad() {
	LoadScript(CONFIG.ASSETS+'/js/flipclock.min.js', function () {
		clock = new FlipClock($('#count-down'), 60, {clockFace: 'Counter'});
	},false)
}

function CountDown(time) {
	if(time > 0) {
		$('#count-down').css('opacity', 1);
		if(btn_chip_confirm_checker === false) {
			
			$('.chip-bw').slideUp({complete: function(){
				$(this).hide();
				$('.chip-box').show({complete: function(){
				$(this).slideDown();
				}});
			}});
		}
		clock.setCounter(time);
	} else {
		$('#count-down').css('opacity', 0);
		$('.chip-box').slideUp({complete: function(){
			$(this).hide();
			$('.chip-bw').show({complete: function(){
				$(this).slideDown();
			}});
		}});
		$('.bet-confirm-box').hide();
		clock.setCounter(40);
	}
}
/************************************************************
 * CardBox
 ***********************************************************/
function CardBox(type) {
	if(type == 'show') {
		$('.card-box-blind').fadeIn();
		$('.card-box').fadeIn();
		$('.cards').fadeIn();
	} else {
		$('.card-box-blind').fadeOut();
		$('.card-box').fadeOut();
		$('.cards').fadeOut();
	}
}
/************************************************************
 * GetMyMoney
 * 이곳에 보유머니 AJAX를 연동해주세요.
 ***********************************************************/
function GetMyMoney() {
	// PostJson('URL', {}, function (returnData) {
	// 	if(returnData.ErrorCode == 0) {
	// 		$('#my-money').text(AddComma(returnData.Data.Money));
	// 		CONFIG.MEMBER.MONEY = returnData.Data.Money;
	// 	} else {
	// 		return 0;
	// 	}
	// })
}

/******************************************************************
 * GET GAME HISTORY
 *****************************************************************/
function GetHistory() {
	PostJson('saisai_json.php?mode=history',{token:'abcd'}, function (returnData) {
		if(returnData.ErrorCode != 0) {
			//alert('데이터 오류로 인해 새로고침이 됩니다.');
			location.reload();
		} else {
			var records = returnData.Data;

			for(var i in records) {
				
				if(records[i]['status'] != '-') {
					var win = records[i]['center_odd'] == 'O' ? 'P' : 'B';

					var json_data = {
						win : win, 
						pair : 'N',
						pair_player : 'N',
						pair_banker : 'N',
						turn : records[i]['id'],
						number : records[i]['center_number']
					}
					RenderHistorySimple(json_data);
					RenderHistorySimpleDetail(json_data);
					RenderHistoryDetail(json_data);
				}
			}
			LoadScript(CONFIG.ASSETS+'/js/iscroll.js', function () {
				scroll_simple = new IScroll('#history-simple', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

				scroll_detail = new IScroll('#history-detail', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	

				scroll_simple_detail = new IScroll('#history-simple-detail', {
					mouseWheel: false,
					scrollbars: false,
					scrollX: true,
					scrollY: false,
					bounce:false
				});	
			});
			
			setTimeout(function () {
				
				if(simple_y > 5) {
					scroll_simple.scrollTo( ( (simple_y-5) * 15) * -1, 0);	
				}

				if(history_pre_data.x > 5) {
					scroll_detail.scrollTo( ( (history_pre_data.x-5) * 20) * -1, 0);	
				}
				
				if(simple_y > 5) {
					scroll_simple.scrollTo( ( (simple_y-5) * 15) * -1, 0);	
				}
				
			},1000)
		}
	});
}

/****************************************************************************************************
 * EVENT
 ***************************************************************************************************/
$('.show-bet-panel').on('click', function () {
	$('.card-box-blind').fadeOut();
	$('.card-box').fadeOut();
	$('.cards').fadeOut();
})

$('.show-card-box').on('click', function () {
	$('.card-box-blind').fadeIn();
	$('.card-box').fadeIn();
	$('.cards').fadeIn();
});

$('.btn-bet').hover(function() {
	$(this).css("background-image",$(this).css("background-image").replace(/\_off\.png/, '\_on\.png'));
}, function() { 
	if($(this).hasClass("btn-bet-temp") == false){
	$(this).css("background-image",$(this).css("background-image").replace(/\_on\.png/, '\_off\.png'));
	}
});

$('.second-btn-box').hover(function() {
	$(this).attr('src',$(this).attr("src").replace(/\_off\.png/, '\_on\.png'));
}, function() { 
	$(this).attr('src',$(this).attr("src").replace(/\_on\.png/, '\_off\.png'));
});


var log_toggle = false;
var log_toggle2 = false;
var log_toggle3 = false;
$('.history-show').on('click', function () {
	log_toggle = true;
	log_toggle2 = true;
	log_toggle3 = true;
	$('.official-list').hide();
	$('.chat-list-box').hide();
	$('.game-desc').hide();
	$('.history-box').toggle();
})

$('.toggle-button').on('click', function () {
	if(log_toggle == false) {
		log_toggle = true;
		$('.chat-list-box').hide();
	} else {
		log_toggle = false;
		log_toggle2 = true;
		log_toggle3 = true;
		$('.official-list').hide();
		$('.history-box').hide();
		$('.game-desc').hide();
		$('.chat-list-box').show();
	}
})
$('.show-desc-game').on('click', function () {
	if(log_toggle3 == false) {
		log_toggle3 = true;
		$('.game-desc').hide();
	} else {
		log_toggle3 = false;
		log_toggle = true;
		log_toggle2 = true;
		$('.official-list').hide();
		$('.history-box').hide();
		$('.chat-list-box').hide();
		$('.game-desc').show();
	}
})
$('.official-show').on('click', function () {
	if(log_toggle2 == false) {
		log_toggle2 = true;
		$('.official-list').hide();
	} else {
		log_toggle2 = false;
		log_toggle = true;
		log_toggle3 = true;
		$('.history-box').hide();
		$('.game-desc').hide();
		$('.chat-list-box').hide();
		$('.official-list').show();
	}
})

$('.chip-btn').on('click', function () {
	SoundPlay('chip');
	$('.chip-btn-money').each(function (index,el) {
		$(el).removeClass('flicker');
	});

	$('.chip-btn').each(function (index,el) {
		$(el).attr('src', $(el).attr('src').replace(/\_on\.png/, '\_off\.png'));
		$(el).removeClass('flicker');
	});
	
	var money = $(this).data('money');
	var money_location = $(this).data('location');
	if($(this).attr('src').indexOf('_on.png') != -1) {
		$(this).attr('src', $(this).attr('src').replace(/\_on\.png/, '\_off\.png'));
		selected_chip = 0;
		$(this).removeClass('flicker');
	} else {
		$(this).attr('src', $(this).attr('src').replace(/\_off\.png/, '\_on\.png'))
		if(money != selected_chip) {
			ChatMessage(AddComma(money)+'칩을 선택하였습니다.','비트윈');
			selected_chip = money;
			selected_chip_location = money_location;
		}
		$(this).addClass('flicker');
	}
})

$('.btn-bet').on('click', function () {
	var options = {
		useEasing: true, 
		useGrouping: true, 
		separator: ',', 
		decimal: '.', 
		};
	$(this).addClass("btn-bet-temp");
	SoundPlay('chip_on');
	var label = $(this).data('label');
	var location = $(this).data('location');
	if(selected_chip <= 0) {
		ChatMessage('<span style="color:#ff5251">칩을 선택해주세요</span>', '비트윈');
	} else {
		if(CONFIG.MEMBER.MONEY < selected_chip) {
			ChatMessage('<span style="color:#ff5251">보유머니가 부족합니다.</span>', '비트윈');
		} else {
			if(special_rate === false)
			{
				ChatMessage('<span style="color:#ff5251">배당이 없음으로 베팅이 불가능합니다.</span>', '비트윈');
				return;
			}

			if( (SumArray(betting_money_arr) + selected_chip) > CONFIG.MEMBER.MONEY) {
				ChatMessage('<span style="color:#ff5251">보유머니가 부족합니다.</span>', '비트윈');
			}
			else
			{
				var before_money = betting_money_arr[location];
				betting_money_arr[location] = betting_money_arr[location] + selected_chip;
				var countup = new CountUp('bet-money-'+location, before_money, betting_money_arr[location], 0, 0.5, options);
				countup.start();
			}	
		}
	}
});

$('.input-box-text').on('keypress', function (event) {
	var message = ($('.input-box-text').val())
	if(event.charCode == 13 || event.keyCode == 13 || event.which == 13) {
		if(message == '' || message.length <=0 ) {
			return;
		}

		PostJson('json.php?mode=chat', {nickname : CONFIG.MEMBER.NICKNAME, message : message}, function (returnData) {
			if(returnData.ErrorCode == 0)
			{
				ChatMessage('메세지 전송(딜레이 3초)', '비트윈');	
			} else {
				ChatMessage('한글,영어,숫자만 가능.', '비트윈');	
			}
			$('.input-box-text').val('')
			ChatScroll();
		});
	}
})

$('.send-button').on('click', function () {
	var message = ($('.input-box-text').val())
	if(message == '' || message.length <=0 ) {return;}
	//ChatMessage('데모버전에서는 채팅을이용할수없습니다.', '채팅');
	PostJson('json.php?mode=chat', {nickname : CONFIG.MEMBER.NICKNAME, message : message}, function (returnData) {
		if(returnData.ErrorCode == 0)
		{
			ChatMessage('메세지 전송(딜레이 3초)', '비트윈');	
		} else {
			ChatMessage('한글,영어,숫자만 가능.', '비트윈');	
		}
		$('.input-box-text').val('')
		ChatScroll();
	});
})

$('.btn-chip-reset').on('click', function () {
	$('.btn-bet-temp').each(function (index, el) {
		$(el).css("background-image", $(el).css("background-image").replace(/\_on\.png/, '\_off\.png'));
	});
	$('.btn-bet-temp').removeClass('btn-bet-temp');
	betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	$('.bet-money').text(0);
	ChatMessage('금액 초기화를 하였습니다.', '비트윈');
	_.each(betting_money_arr_confirm, function (row,index) {
		$('.bet-money-'+index).text(AddComma(betting_money_arr_confirm[index]));
	});
})

$('.btn-chip-confirm').on('click', function () {
	$('.btn-bet-temp').each(function (index, el) {
		$(el).addClass("flicker_three");
	});
	PostJson('saisai_json.php?mode=betting', {bettings : betting_money_arr}, function (returnData) {
		if(returnData.ErrorCode != 0)
		{
			ChatMessage(returnData.Message, '비트윈');
		}
		else
		{
			_.each(betting_money_arr, function (row,index) {
				if(index == 0) {
					if(row > 0) {
						ChatMessage(betting_location_label[special_loc]+'위치에 '+AddComma(row)+'원을 베팅 성공하였습니다.', '비트윈');	
					}
				} else {
					if(row > 0) {
						ChatMessage(betting_location_label[index]+'위치에 '+AddComma(row)+'원을 베팅 성공하였습니다.', '비트윈');	
					}
					
				}
			});
			betting_confirm = true;
			_.each(betting_money_arr_confirm, function (row,index) {
				betting_money_arr_confirm[index] =betting_money_arr_confirm[index] + betting_money_arr[index];
				
			});

			betting_money_arr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			_.each(betting_money_arr_confirm, function (row,index) {
				if(row > 0) {
					$('.bet-money-'+index).text( AddComma(betting_money_arr_confirm[index]));
				}
			});
			$('.bet-confirm-box').fadeOut();
			btn_chip_confirm_checker = false;
		}
	});
	setTimeout(function(){
		$('.btn-bet-temp').each(function (index, el) {
			$(el).removeClass("flicker_three");
		});
	},1700);
});

function ToggleWinner(oe, number, pattern, color, left_number, right_number) {
	$('.btn-bet').each(function (index, el) {
		$(el).removeClass("flicker").css("background-image", $(el).css("background-image").replace(/\_on\.png/, '\_off\.png'));
	});
	$('.win-'+oe.toLowerCase()).addClass("flicker").css("background-image",$('.win-'+oe.toLowerCase()).css("background-image").replace(/\_off\.png/, '\_on\.png'));
	$('.win-c-'+color.toLowerCase()).addClass("flicker").css("background-image",$('.win-c-'+color.toLowerCase()).css("background-image").replace(/\_off\.png/, '\_on\.png'));
	$('.win-'+pattern.toLowerCase()).addClass("flicker").css("background-image",$('.win-'+pattern.toLowerCase()).css("background-image").replace(/\_off\.png/, '\_on\.png'));
	
	if(parseInt(number) >= 1 && parseInt(number) <= 4) {
		$('.win-low').addClass("flicker").css("background-image",$('.win-low').css("background-image").replace(/\_off\.png/, '\_on\.png'));
	} else if(parseInt(number) >= 5 && parseInt(number) <= 8) {
		$('.win-middle').addClass("flicker").css("background-image",$('.win-middle').css("background-image").replace(/\_off\.png/, '\_on\.png'));
	} else if(parseInt(number) >= 9 && parseInt(number) <= 12) {
		$('.win-high').addClass("flicker").css("background-image",$('.win-high').css("background-image").replace(/\_off\.png/, '\_on\.png'));
	}
	var numbers = [left_number, right_number];
	var calculate = numbers.sort(function(a, b) {
		return a - b;
	});
	console.log(calculate[0], calculate[1], number)
	//고배당 12.5
	if(calculate[0] == calculate[1]) {
		if(calculate[0] == parseInt(number))
		{
			$('.btn-special').addClass("flicker").css("background-image",$('.btn-special').css("background-image").replace(/\_off\.png/, '\_on\.png'));
		}
	} else {
		if(calculate[0] < parseInt(number) && calculate[1] > parseInt(number) ) {
			console.log('OK!')
			$('.btn-special').addClass("flicker").css("background-image",$('.btn-special').css("background-image").replace(/\_off\.png/, '\_on\.png'));
		}	
	}
}

var PlayLoop = setInterval(function () {
	PostJson('saisai_json.php?mode=play',{}, function (returnData) {
		if(returnData.ErrorCode != 0) {
			location.reload();
		} else {
			GetMyMoney();
			var data = returnData.Data;
			if(data == null) {
				return;
			}
			
			status = data['status'];

			if(data['shuffle'] == 'Y')
			{
				$('.flicker').each(function (index, el) {
					$(el).css("background-image", $(el).css("background-image").replace(/\_on\.png/, '\_off\.png'));
				});
				$('.flicker').removeClass("flicker");
				NotificationMessage('카드 셔플중입니다.. 약 5분동안 진행됩니다.');
			}

			if(turn == 0 ) {
				turn = data['turn'];
			}

			if(turn != data['turn']) {
				turn = data['turn'];
				$('.left-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/cardback2.png")').removeClass('left-card-show');
				$('.center-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/cardback2.png")').removeClass('center-card-show');
				$('.right-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/cardback2.png")').removeClass('right-card-show');
				$('.bet-money').text(0);
			}
			
			if(data['rate'] === false) {
				special_rate = false;
				special_loc = false;
				$('.special-rate').text('배당없음');
			} else {
				special_rate = special_rates[data['rate']];
				special_loc = data['rate'];
				$('.special-rate').text(special_rates[data['rate']]);
			}

			//COUNT
			if(data['count'] > 0) {
				if(first_mesage_checker === false) {
					first_mesage_checker = true;
					//ChatMessage(data['turn'] +'회차 베팅시작 되었습니다.', '비트윈');
				}

				if(data['count'] <= 10 && data['count'] >= 1) {
					SoundPlay(data['count']);
				}

				if(first_notification_checker === false) {
					first_notification_checker = true;
					NotificationMessage(AddComma(data['turn']) +'회차 베팅시작 되었습니다.', '비트윈')
					ChatMessage(AddComma(data['turn']) +'회차 베팅시작 되었습니다.', '비트윈')
					SoundPlay('betting_start');
					CardBox('hide');
					
					$('.btn-bet').each(function (index, el) {
						$(el).removeClass("flicker").css("background-image", $(el).css("background-image").replace(/\_on\.png/, '\_off\.png'));
					});
				}
				CountDown(data['count'])	
			} else {
				if(first_mesage_checker === true) {
					first_mesage_checker = false;
					$('.btn-bet-temp').removeClass("btn-bet-temp");
					NotificationMessage(AddComma(data['turn']) +'회차 베팅종료 되었습니다.');
					ChatMessage(AddComma(data['turn']) +'회차 베팅종료 되었습니다.', '비트윈');
					SoundPlay('betting_end');
					CardBox('show');
					$('.btn-bet-temp').removeClass("btn-bet-temp");
					$('.card-box').fadeIn();
					$('.cards').fadeIn();
				}
				CountDown(0);
			}

			if(data['center_card'] !== false) {
				$('.center-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['center_card'].toLowerCase()+'.png")');
				$('.center-card').addClass('center-card-show');
			}

			if(data['left_card'] !== false) {
				$('.left-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['left_card'].toLowerCase()+'.png")');
				$('.left-card').addClass('left-card-show');
				if(parseInt(data['left_number']) == 1) {
					$('.left-number').text('A');	
				} else if(parseInt(data['left_number']) == 11) {
					$('.left-number').text('J');
				} else if(parseInt(data['left_number']) == 12) {
					$('.left-number').text('Q');
				} else {
					$('.left-number').text(data['left_number']);
				}
			}

			if(data['right_card'] !==  false) {
				$('.right-card').css('background-image','url("'+CONFIG.ASSETS+'/img/card/'+data['right_card'].toLowerCase()+'.png")');
				$('.right-card').addClass('right-card-show');
				if(parseInt(data['right_number']) == 1) {
					$('.right-number').text('A');	
				} else if(parseInt(data['right_number']) == 11) {
					$('.right-number').text('J');
				} else if(parseInt(data['right_number']) == 12) {
					$('.right-number').text('Q');
				} else {
					$('.right-number').text(data['right_number']);
				}
			}

			if(data['status'] == 'COMPLETED' && data['result'] != '-') {
				if($('.myb-'+data['turn']).length <= 0) {
					var win = data['center_odd'] == 'O' ? 'P' : 'B';

					var json_data = {
						win : win, 
						pair : 'N',
						turn : data['turn'],
						number : data['center_number']
					}
					console.log(json_data);
					RenderHistoryDetail(json_data);
					setTimeout(function () {
						if(simple_y > 5) {
							scroll_detail.scrollTo( ( (simple_y-5) * 15) * -1, 0);	
						}

						if(history_pre_data.x > 5) {
							scroll_detail.scrollTo( ( (history_pre_data.x-5) * 20) * -1, 0);	
						}
					},1000);
					ToggleWinner(data['center_odd'], data['center_number'], data['center_pattern'], data['center_color'], data['left_number'], data['right_number']);
					first_notification_checker = false;
					betting_money_arr_confirm = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
				}
			} else {
			}
		}
	})
},1000)

ClockLoad();
MakeHistoryDetailMap();
DrawHistoryBackground();
GetHistory();

setTimeout(function () {
	$('.toggle-button').click();
	$('.history-show').click();
},2000)
})