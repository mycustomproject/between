/* 로그 기록시 안정성을 위하여 */
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());

//콤마 추가.
function AddComma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

//콤마 삭제.
function DelComma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
}

//AJAX 수신.
function PostJson(PostUrl, JsonData, CallBack, syncWork)
{
	if (typeof(syncWork)=="undefined")
	{
		syncWork=true;
	}

	$.ajax({
		url: PostUrl,
		cache:false,
		async:syncWork,
		data: JsonData,
		dataType: "json",
		beforeSend: function (){},
		success: function(ReturnData) {
			if(CallBack) {
				CallBack(ReturnData);	
			}
			
		},
		error : function () {
			
		},
		timeout: 10000,
		type: "POST"
	});
}

function SetWon(pWon) {
    var won = GetNumber(pWon);
    var arrWon = ["원", "만", "억", "조", "경", "해", "자", "양", "구", "간", "정"];
    var changeWon = "";
    var pattern = /(-?[0-9]+)([0-9]{4})/;
    while (pattern.test(won)) {
        won = won.replace(pattern, "$1,$2");
    }
    var arrCnt = won.split(",").length - 1;
    for (var ii = 0; ii < won.split(",").length; ii++) {
        if (arrWon[arrCnt] == undefined) {
            alert("값의 수가 너무 큽니다.");
            break;
        }
        var tmpwon = 0;
        for (i = 0; i < won.split(",")[ii].length; i++) {
            var num1 = won.split(",")[ii].substring(i, i + 1);
            tmpwon = tmpwon + Number(num1);
        }
        if (tmpwon > 0) {
            changeWon += won.split(",")[ii] + arrWon[arrCnt]; //55억0000만0000원이런 형태 방지 0000 다 짤라 버린다
        }
        arrCnt--;
    }
    return changeWon;
}
function LeadingZeros(n, digits) {
	var zero = '';
	n = n.toString();

	if (n.length < digits) {
		for (i = 0; i < digits - n.length; i++)
			zero += '0';
	}

	return zero + n;
}

function CutStr(str,limit,dot_type){
	if(typeof(dot_type) == 'undefined') {
		dot_type = false;
	}
	var tmpStr = str;
	var byte_count = 0;
	var len = str.length;
	var dot = "";
	for(i=0; i<len; i++){
		byte_count += charByte(str.charAt(i));
		if(byte_count == limit-1){
			if(charByte(str.charAt(i+1)) == 2){
				tmpStr = str.substring(0,i+1);
				dot = (dot_type == false) ? '...' : '***';
			} else {
				if(i+2 != len){
					dot = (dot_type == false) ? '...' : '***';
				}
				tmpStr = str.substring(0,i+2);
			}
			break;
		} else if(byte_count == limit){
			if(i+1 != len){
				dot = (dot_type == false) ? '...' : '***';
			}
			tmpStr = str.substring(0,i+1);
			break;
		}
	}

	return tmpStr+dot;
}
//for requiring a script loaded asynchronously.
function LoadScript(src, callback, relative){
    var baseUrl = CONFIG.ASSETS;
    var script = document.createElement('script');
    if(relative === true){
        script.src = baseUrl + src;  
    }else{
        script.src = src; 
    }

    if(callback !== null){
        if (script.readyState) { // IE, incl. IE9
            script.onreadystatechange = function() {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {
            script.onload = function() { // Other browsers
                callback();
            };
        }
    }
    document.getElementsByTagName('head')[0].appendChild(script);
}
function GetNumber(str){
    var res;
    res = str.replace(/[^0-9]/g,"");
    return res;
}
//EXAMPLE
/*
loadAsync('https://www.gstatic.com/charts/loader.js' , function(){
    chart.loadCharts();
    });
// OR relative path
loadAsync('fastclick.js', null, true);
 */

function SumArray(array) {
  for (
    var
      index = 0,              // The iterator
      length = array.length,  // Cache the array length
      sum = 0;                // The total amount
      index < length;         // The "for"-loop condition
      sum += array[index++]   // Add number on each iteration
  );
  return sum;
}