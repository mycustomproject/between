<?php
	$usr_agent = $_SERVER['HTTP_USER_AGENT'];
	$is_iphone = preg_match("~\biphone\b~",strtolower($usr_agent));
	$is_ipad = preg_match("~\bipad\b~",strtolower($usr_agent));
	$is_android = preg_match("~\bandroid\b~",strtolower($usr_agent));
?>
 <!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="Content-Security-Policy" content="default-src * data:; style-src * 'unsafe-inline'; script-src * 'unsafe-inline' 'unsafe-eval'">
<title>DEMO CLIENT</title>
<link rel="stylesheet" type="text/css" media="screen" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="assets/css/flipclock.css?v=<?php echo time()?>">
<link rel="stylesheet" href="assets/css/style.css?v=<?php echo time()?>">
<link rel="stylesheet" href="assets/css/baccarat/pc.css?v=<?php echo time()?>">
</head>
<body>
	<div class="bacc-wrap bg-color-black">
		<div class="video">
			<div class="message-box" style="background-image: url('assets/img/background/message_box.png')"></div>
			<?php if($is_iphone == 0 && $is_ipad == 0 && $is_android == 0) {?>
			<object id="video_swf" type="application/x-shockwave-flash" data="assets/swf/baccarat.swf" width="100%" height="100%">
				<param name="movie" value="assets/swf/baccarat.swf">
				<param name="quality" value="high">
				<param name="bgcolor" value="#101010">
				<param name="play" value="true">
				<param name="loop" value="true">
				<param name="wmode" value="window">
				<param name="scale" value="showall">
				<param name="menu" value="true">
				<param name="devicefont" value="false">
				<param name="salign" value="">
				<param name="allowScriptAccess" value="sameDomain">
			</object>
			<?php } else {?>
			<canvas id="videoCanvas" class="video-mobile"></canvas>
			<script src="assets/js/mpeg.js"></script>
			<script>
				var canvas = document.getElementById('videoCanvas');
				var ctx = canvas.getContext('2d');
				ctx.fillStyle = '#444';
				ctx.fillText('Stream loading...', canvas.width/2-30, canvas.height/3);
				var client = new WebSocket( 'wss://video.8008.company/' );
				var player = new jsmpeg(client, {canvas:canvas});
			</script>
			<?php }?>
			<div id="count-down"></div>

			<div class="win-box">
				<img src="assets/img/win/winner_t.png" alt="">
			</div>


			<div class="bet-confirm-box" style="background-image:url('assets/img/background/bet_confirm_box_2.png?v=300')">
				<div class="message">
					베팅을 하시겠습니까?
				</div>
				<div class="btn-chip-ok">
					<img src="assets/img/button/baccarat/bet_confirm_off.png?v=300">
				</div>

				<div class="btn-chip-cancel">
					<img src="assets/img/button/baccarat/bet_cancel_off.png?v=300">
				</div>
			</div>
			

			<div class="chat-list-box" id="chat-list-box">
				<ul>
					
				</ul>

			</div>
			<div class="official-list">

			</div>
		</div>
		<div class="baccarat-title">
			<div class="title">
				<strong>[비트윈]</strong>님 환영합니다.
			</div>
			<div class="buttonBox">
				<div class="history-show button">
					출목표 통계
				</div>
				<div class="toggle-button button">
					배팅 로그
				</div>
				<div class="official-show button">
					공식 결과
				</div>
			</div>
			<div class="money">
				보유머니 : 1,000,000,000원
			</div>
		</div>
		
		<div class="history-box">
			

			<div class="simple-history-box" id="history-simple" style="background-image:url('assets/img/history/background.png?v=<?php echo time()?>')">
			</div>

			<div class="detail-history-box" id="history-detail" style="background-image:url('assets/img/history/background.png?v=<?php echo time()?>')">
			</div>

			<div class="empty-history-box" id="history-number" style="background-image:url('assets/img/history/background.png?v=1514019898')">
			   <div class="resultNumFirst">
				  <div style="background: #004bd6;">P - <span class="count-p">0</span></div>
				  <div style="background: #009607;">T - <span class="count-t">0</span></div>
				  <div style="background: #e20404;">B - <span class="count-b">0</span></div>
			   </div>
			   <div class="resultNumSecond">
				  <div style="background: #004bd6;">PP - <span class="count-pp">0</span></div>
				  <div style="background: #e20404;">BB - <span class="count-bp">0</span></div>
			   </div>
			</div>
			
			<div class="detail-simple-history-box" id="history-simple-detail" style="background-image:url('assets/img/history/background.png?v=<?php echo time()?>')">
			</div>
			<div class="ch-tmp-point">
				비트윈 공식 결과 및 API <a href="https://www.betweenapi.com" target="_blank" style="color:#fff">HTTPS://WWW.BETWEENAPI.COM</a>
			</div>
		</div>
		<div class="baccarat-chat-box">
			<div class="title">
				비트윈 라이브 채팅
			</div>
			<div class="input-box">
				<input type="text" maxlength="8" placeholder="(해당 기능은 비트윈 바카라가 라이브임을 인증하는 기능이며, 실제 현장과의 딜레이는 3초이내이기 때문에 조작이 불가능합니다.)" class="input-box-text">
			</div>
			<div class="send-button">
				전송
			</div>
		</div>

		<div class="betting-panel" style="display:none !important">
			<div class="betting-btn-box">
				<div class="btn-bet btn-player-pair btn-panel-4 win-pair-player" style="background-image:url('assets/img/button/baccarat/player_pair_off.png?v=<?php echo time()?>')" data-label="플레이어 페어" data-location="4">
					<div class="rate">11.00</div>
					<div class="bet-money-chip bet-money-chip-4 win-pp" 
					style="background-image: url(/assets/img/chip/panel_chip_1.png);"></div>
					<div class="chip-spead-4 chip-spread" style="background-image: url(/assets/img/chip/chip_spread_pair.png);"></div>
					<div class="bet-money bet-money-4" id="bet-money-4">0</div>
				</div>
				<div class="btn-bet btn-player btn-panel-1 win-p" style="background-image:url('assets/img/button/baccarat/player_off.png?v=<?php echo time()?>')" data-label="플레이어"  data-location="1">
					<div class="rate">2.00</div>
					<div class="bet-money-chip bet-money-chip-1" 
					style="background-image: url(/assets/img/chip/panel_chip_1.png);"></div>
					<div class="chip-spead-1 chip-spread" style="background-image: url(/assets/img/chip/chip_spread.png);"></div>
					<div class="bet-money bet-money-1" id="bet-money-1">0</div>
				</div>
				<div class="btn-bet btn-tie btn-panel-3 win-t" style="background-image:url('assets/img/button/baccarat/tie_off.png?v=<?php echo time()?>')" data-label="타이"  data-location="3">
					<div class="rate">
						8:1
					</div>
					<div class="bet-money-chip bet-money-chip-3" 
					style="background-image: url(/assets/img/chip/panel_chip_1.png);"></div>
					<div class="chip-spead-3 chip-spread" style="background-image: url(/assets/img/chip/chip_spread_tie.png);"></div>
					<div class="bet-money bet-money-3" id="bet-money-3">0</div>
				</div>
				<div class="btn-bet btn-banker btn-panel-2 win-b" style="background-image:url('assets/img/button/baccarat/banker_off.png?v=<?php echo time()?>')" data-label="뱅커"  data-location="2">
					<div class="rate">
						1.95
					</div>
					<div class="bet-money-chip bet-money-chip-2" 
					style="background-image: url(/assets/img/chip/panel_chip_1.png);"></div>
					<div class="chip-spead-2 chip-spread" style="background-image: url(/assets/img/chip/chip_spread.png);"></div>
					<div class="bet-money bet-money-2" id="bet-money-2">0</div>
				</div>
				<div class="btn-bet btn-banker-pair btn-panel-5 win-pair-banker win-bp" style="background-image:url('assets/img/button/baccarat/banker_pair_off.png?v=<?php echo time()?>')" data-label="뱅커 페어" data-location="5">
					<div class="rate">
						11.00
					</div>
					<div class="bet-money-chip bet-money-chip-5" 
					style="background-image: url(/assets/img/chip/panel_chip_1.png);"></div>
					<div class="chip-spead-5 chip-spread" style="background-image: url(/assets/img/chip/chip_spread_pair.png);"></div>
					<div class="bet-money bet-money-5" id="bet-money-5">0</div>
				</div>
			</div>

			<div class="card-box-blind">
			</div>
			<div class="card-box">
				<div class="card_players card-player-1" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time()?>')"></div>
				<div class="card_players card-player-2" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time()?>')"></div>
				<div class="card_players card-player-3" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time()?>')"></div>
				
				<div class="sum-box player-sum">
					0
				</div>

				<div class="card_bankers card-banker-1" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time()?>')"></div>
				<div class="card_bankers card-banker-2" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time()?>')"></div>
				<div class="card_bankers card-banker-3" style="background-image:url('assets/img/card/cardback2.png?v=<?php echo time()?>')"></div>

				<div class="sum-box banker-sum">
					0
				</div>
			</div>
		</div>

		
			
	</div>

	
	<script>
		var CONFIG = {
			ASSETS : 'assets',
			AJAX : 'json.php',
			MEMBER : {
				IS_LOGIN:<?php echo $member == null ? 'false' : 'true'?>,
				NICKNAME : '게스트',
				MONEY : 100000000
			},
			GAME : {
				MIN : 10000,
				MAX : 10000000
			}
		}
	</script>
	
	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/underscore.js"></script>
	<script src="assets/js/count.js"></script>
	<script src="assets/js/function.js?v=<?php echo time()?>"></script>
	<script src="assets/js/baccarat/pc.js?v=<?php echo time()?>"></script>
</body>
</html>
